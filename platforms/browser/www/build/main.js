webpackJsonp([12],{

/***/ 109:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__invoice_invoice__ = __webpack_require__(33);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.passwordType = 'password';
        this.passwordShown = false;
    }
    LoginPage.prototype.togglePasword = function () {
        if (this.passwordShown) {
            this.passwordShown = false;
            this.passwordType = 'password';
        }
        else {
            this.passwordShown = true;
            this.passwordType = 'text';
        }
    };
    LoginPage.prototype.invoicePg = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__invoice_invoice__["a" /* InvoicePage */]);
        // alert('it works')
    };
    LoginPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginPage');
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"/home/austinwise1993/Documents/HealthPayment/src/pages/login/login.html"*/'<ion-content padding class="bgImg">\n    \n    <div class="conEdit">\n       <h2>Health Pay</h2>\n       <p><b>Welcome,</b> Sign in to continue</p>\n    </div>\n \n     <ion-card>\n         <ion-card-content>\n             <ion-list>              \n \n                 <ion-item>\n                   <ion-icon color="wise" ios="ios-person" md="md-person" item-left></ion-icon>\n                   <ion-label floating>Username</ion-label>\n                   <ion-input type="text" value=""></ion-input>\n                 </ion-item>\n               \n                 <ion-item>\n                   <ion-icon color="wise" ios="ios-lock" md="md-lock" item-left></ion-icon>\n                   <ion-label floating>Password</ion-label>\n                   <ion-input [type]="passwordType"></ion-input>\n                   <ion-icon name="eye" [color]="passwordShown === true ? \'dark\' : \'gray\'"  item-end (click)="togglePasword()"></ion-icon>\n                 </ion-item>\n               </ion-list>\n \n         </ion-card-content>\n \n         <button class="signin-btn" ion-button block color="blue" (click)="invoicePg()">\n             <span>Sign In </span> \n         </button>\n \n       </ion-card>\n </ion-content>'/*ion-inline-end:"/home/austinwise1993/Documents/HealthPayment/src/pages/login/login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 110:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DevicesModalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__insertcard_insertcard__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__enter_pin_enter_pin__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__results_results__ = __webpack_require__(55);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


// import { PaymentPage } from '../payment/payment';



var DevicesModalPage = /** @class */ (function () {
    function DevicesModalPage(modalCtrl, navCtrl, alertCtrl, loading, navParams, viewCtrl) {
        this.modalCtrl = modalCtrl;
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.loading = loading;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        // back() {
        //   this.navCtrl.push(PaymentPage);
        // }
        this.dataDevices = [];
    }
    DevicesModalPage.prototype.ionViewWillEnter = function () {
        this.viewCtrl.showBackButton(true);
    };
    DevicesModalPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DevicesModalPage');
        // console.log(JSON.stringify(this.navParams.get('obj')));
        this.deviceList = this.navParams.get('obj');
    };
    // ionViewDidEnter() {
    //   this.navBar.backButtonClick = () => {
    //     this.navCtrl.push(PaymentPage);
    //   };
    // }
    DevicesModalPage.prototype.bluetoothfunc = function (name, address) {
        var _this = this;
        console.log('bluetooth address', address);
        console.log("Got here");
        var loader = this.loading.create({
            content: '<ion-spinner name="dots"></ion-spinner> <br />',
        });
        PaypadFacade.connection(address, function (response) {
            console.log('status ', response.status, 'address', address);
            var status = response.status;
            if (status) {
                if (status === "enabling_bluetooth") {
                    loader = _this.loading.create({
                        content: '<ion-spinner name="dots"></ion-spinner> <br />Please wait enabling bluetooth...',
                    });
                    loader.present();
                }
                else if (status === "connecting") {
                    loader.dismiss();
                    console.log('connecting ', status);
                    loader = _this.loading.create({
                        content: '<ion-spinner name="dots"></ion-spinner> <br /> Connecting to pinpad...',
                    });
                    loader.present();
                }
                else if (status === "connected") {
                    loader.dismiss();
                    console.log("connected from devices page");
                    localStorage.setItem("agree", "true");
                    // var x = "2030ES03";
                    var x = "2063073T";
                    // alert('the terminal Id is ' + x);
                    var selectedbank = "Esl";
                    PaypadFacade.initialization(x, true, selectedbank, function (response) {
                        console.log(response);
                        var status = response.status;
                        // alert('Initialization status '+ status);
                        if (status) {
                            if (status === "downloading_keys") {
                                loader = _this.loading.create({
                                    content: '<ion-spinner name="dots"></ion-spinner> <br />Please wait, downloading keys...',
                                });
                                loader.present();
                            }
                            else if (status === "loading_keys_to_pinpad") {
                                loader.dismiss();
                                var alert_1 = _this.alertCtrl.create({
                                    title: 'Status',
                                    subTitle: 'KEY DOWNLOAD SUCCESSFUL',
                                    buttons: ['Dismiss']
                                });
                                alert_1.present();
                                localStorage.setItem("initialization", "true");
                                console.log('called payment from devices page');
                                _this.payment();
                            }
                            else if (status == "Already_initialised") {
                                alert("already initialized");
                            }
                        }
                    }, function (err) {
                        console.log(err);
                        loader.dismiss();
                        var alert = _this.alertCtrl.create({
                            title: 'Status',
                            subTitle: 'Unable to Download Keys',
                            buttons: ['Dismiss']
                        });
                        alert.present();
                        localStorage.setItem("initialization", "false");
                    });
                }
                else {
                    loader.dismiss();
                }
            }
        }, function (response) {
            // alert("Error "+JSON.stringify(response));
            loader.dismiss();
            localStorage.setItem("initialization", "false");
            var alert = _this.alertCtrl.create({
                title: 'Status',
                subTitle: 'Unable to Connect to Pinpad',
                buttons: ['Dismiss']
            });
            alert.present();
        });
    };
    ;
    DevicesModalPage.prototype.payment = function () {
        // console.log("Got here")
        var _this = this;
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__insertcard_insertcard__["a" /* InsertcardPage */]);
        var loader = this.loading.create({
            content: '<ion-spinner name="dots"></ion-spinner> <br />',
        });
        PaypadFacade.payment({ "amount": localStorage.getItem('Amount'), "account": "savings" }, function (data) {
            // console.log("data from payment")
            // console.log(data);
            if (data.operation === "payment") {
                if (data.status === "processing") {
                    loader = _this.loading.create({
                        content: '<ion-spinner name="dots"></ion-spinner> <br />Processing...',
                    });
                    loader.present();
                }
                else if (data.status === "enterPin") {
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__enter_pin_enter_pin__["a" /* EnterPinPage */]);
                    loader.dismiss();
                }
                else {
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__results_results__["a" /* ResultsPage */], { "obj": data });
                    loader.dismiss();
                }
            }
        }, function (error) {
            loader.dismiss();
            console.log(error);
        });
    };
    ;
    DevicesModalPage.prototype.ionViewDidEnter = function () {
        this.navBar.backButtonClick = function () {
        };
    };
    DevicesModalPage.prototype.doRefresh = function (refresher) {
        console.log('Started', refresher);
        setTimeout(function () {
            console.log('Async operation has ended');
            refresher.complete();
        }, 4000);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('navbar'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Navbar */])
    ], DevicesModalPage.prototype, "navBar", void 0);
    DevicesModalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-devices-modal',template:/*ion-inline-start:"/home/austinwise1993/Documents/HealthPayment/src/pages/devices-modal/devices-modal.html"*/'\n<ion-header>\n  <ion-navbar>\n      <!-- <ion-buttons left>\n        <button ion-button navPop icon-only>\n          <ion-icon ios="ios-arrow-back" md="nbsons-arrow-back"></ion-icon>\n        </button>\n      </ion-buttons> -->\n    <ion-title>Select PinPad</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content #navbar padding>\n  \n  <ion-refresher (ionRefresh)="doRefresh($event)">\n    <ion-refresher-content\n      pullingIcon="arrow-dropdown"\n      pullingText="Pull Down."\n      refreshingSpinner="circles"\n      refreshingText="Loading...">\n    </ion-refresher-content>\n  </ion-refresher>\n\n    <div>\n        <ul>\n          <li style="font-size:13px;" *ngFor="let device of deviceList" style="padding:10px">\n            <a style="display:block; padding:6px 0;" (click)="bluetoothfunc(device.name, device.address)"> {{device.name}}\n            </a>\n          </li>\n        </ul>\n        <br>\n    </div>\n    \n</ion-content>\n'/*ion-inline-end:"/home/austinwise1993/Documents/HealthPayment/src/pages/devices-modal/devices-modal.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */]])
    ], DevicesModalPage);
    return DevicesModalPage;
}());

//# sourceMappingURL=devices-modal.js.map

/***/ }),

/***/ 120:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 120;

/***/ }),

/***/ 162:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/activation/activation.module": [
		293,
		11
	],
	"../pages/confirm/confirm.module": [
		294,
		10
	],
	"../pages/devices-modal/devices-modal.module": [
		295,
		9
	],
	"../pages/enter-pin/enter-pin.module": [
		296,
		8
	],
	"../pages/insertcard/insertcard.module": [
		297,
		7
	],
	"../pages/invoice/invoice.module": [
		298,
		6
	],
	"../pages/login/login.module": [
		299,
		5
	],
	"../pages/notification/notification.module": [
		300,
		1
	],
	"../pages/payment/payment.module": [
		301,
		4
	],
	"../pages/results/results.module": [
		302,
		3
	],
	"../pages/select-account-type/select-account-type.module": [
		303,
		0
	],
	"../pages/success/success.module": [
		304,
		2
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 162;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 206:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ActivationPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login_login__ = __webpack_require__(109);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ActivationPage = /** @class */ (function () {
    function ActivationPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ActivationPage.prototype.loginPg = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__login_login__["a" /* LoginPage */]);
        // alert('it works');
    };
    ActivationPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ActivationPage');
    };
    ActivationPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-activation',template:/*ion-inline-start:"/home/austinwise1993/Documents/HealthPayment/src/pages/activation/activation.html"*/'<ion-content padding class="bgImg">\n    \n    <div class="conEdit">\n       <h2>Health Pay</h2>\n       <p><b>Welcome,</b> J. Damian </p>\n    </div>\n \n     <ion-card>\n         <ion-card-content>\n           <span class="sp"><b>Enter your activation code below</b></span>\n             <ion-list>                             \n                 <ion-item>\n                   <ion-icon ios="ios-lock" md="md-lock" item-left></ion-icon>\n                   <ion-label floating> Activation Code </ion-label>\n                   <ion-input type="text">\n                      \n                   </ion-input>\n                 </ion-item>\n               </ion-list>\n \n         </ion-card-content>\n \n         <button class="signin-btn" ion-button block color="blue" (click)="loginPg()">\n             <span>Proceed </span> \n         </button>\n \n       </ion-card>\n </ion-content>\n '/*ion-inline-end:"/home/austinwise1993/Documents/HealthPayment/src/pages/activation/activation.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */]])
    ], ActivationPage);
    return ActivationPage;
}());

//# sourceMappingURL=activation.js.map

/***/ }),

/***/ 207:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SuccessPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__invoice_invoice__ = __webpack_require__(33);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SuccessPage = /** @class */ (function () {
    function SuccessPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    SuccessPage.prototype.returnHome = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__invoice_invoice__["a" /* InvoicePage */]);
        // alert('it works');
    };
    SuccessPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SuccessPage');
    };
    SuccessPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-success',template:/*ion-inline-start:"/home/austinwise1993/Documents/HealthPayment/src/pages/success/success.html"*/'<ion-header no-border>\n  <ion-toolbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title><img src="../../assets/icon/icon.svg" alt="Smiley face" height="42" width="42">\n    </ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="notify()">\n        <ion-icon name="notifications"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content padding>\n    <div class="payEdit">\n        <img src="../../assets/icon/success.svg" alt="hygieia" height="" width="">\n        <p >Payment Successful</p>\n    </div>\n\n    <button class="signin-btn" ion-button block color="blue" (click)="returnHome()">\n        <span>Return Home </span> \n    </button>\n</ion-content>\n'/*ion-inline-end:"/home/austinwise1993/Documents/HealthPayment/src/pages/success/success.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */]])
    ], SuccessPage);
    return SuccessPage;
}());

//# sourceMappingURL=success.js.map

/***/ }),

/***/ 208:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(209);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(229);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 229:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(283);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_insertcard_insertcard__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_success_success__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_activation_activation__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_confirm_confirm__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_invoice_invoice__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_payment_payment__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_login_login__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_status_bar__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ionic_native_splash_screen__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__angular_common_http__ = __webpack_require__(85);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_devices_modal_devices_modal__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_results_results__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_enter_pin_enter_pin__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__ionic_native_http_ngx__ = __webpack_require__(163);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

















// import { SelectAccountTypePage } from '../pages/select-account-type/select-account-type';

var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_4__pages_insertcard_insertcard__["a" /* InsertcardPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_enter_pin_enter_pin__["a" /* EnterPinPage */],
                __WEBPACK_IMPORTED_MODULE_5__pages_success_success__["a" /* SuccessPage */],
                __WEBPACK_IMPORTED_MODULE_6__pages_activation_activation__["a" /* ActivationPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_confirm_confirm__["a" /* ConfirmPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_invoice_invoice__["a" /* InvoicePage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_devices_modal_devices_modal__["a" /* DevicesModalPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_payment_payment__["a" /* PaymentPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_results_results__["a" /* ResultsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/activation/activation.module#ActivationPageModule', name: 'ActivationPage', segment: 'activation', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/confirm/confirm.module#ConfirmPageModule', name: 'ConfirmPage', segment: 'confirm', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/devices-modal/devices-modal.module#DevicesModalPageModule', name: 'DevicesModalPage', segment: 'devices-modal', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/enter-pin/enter-pin.module#EnterPinPageModule', name: 'EnterPinPage', segment: 'enter-pin', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/insertcard/insertcard.module#InsertcardPageModule', name: 'InsertcardPage', segment: 'insertcard', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/invoice/invoice.module#InvoicePageModule', name: 'InvoicePage', segment: 'invoice', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/notification/notification.module#NotificationPageModule', name: 'NotificationPage', segment: 'notification', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/payment/payment.module#PaymentPageModule', name: 'PaymentPage', segment: 'payment', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/results/results.module#ResultsPageModule', name: 'ResultsPage', segment: 'results', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/select-account-type/select-account-type.module#SelectAccountTypePageModule', name: 'SelectAccountTypePage', segment: 'select-account-type', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/success/success.module#SuccessPageModule', name: 'SuccessPage', segment: 'success', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_13__angular_common_http__["b" /* HttpClientModule */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_4__pages_insertcard_insertcard__["a" /* InsertcardPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_enter_pin_enter_pin__["a" /* EnterPinPage */],
                __WEBPACK_IMPORTED_MODULE_5__pages_success_success__["a" /* SuccessPage */],
                __WEBPACK_IMPORTED_MODULE_6__pages_activation_activation__["a" /* ActivationPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_confirm_confirm__["a" /* ConfirmPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_invoice_invoice__["a" /* InvoicePage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_devices_modal_devices_modal__["a" /* DevicesModalPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_payment_payment__["a" /* PaymentPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_results_results__["a" /* ResultsPage */],
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_11__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_12__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_13__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_17__ionic_native_http_ngx__["a" /* HTTP */],
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 283:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_observable_timer__ = __webpack_require__(284);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_observable_timer___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_observable_timer__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_invoice_invoice__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_payment_payment__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_confirm_confirm__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var MyApp = /** @class */ (function () {
    // data: { title: string; description: string; date: string; time: string; };
    function MyApp(platform, statusBar, splashScreen, app, alertCtrl) {
        var _this = this;
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.app = app;
        this.alertCtrl = alertCtrl;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_5__pages_invoice_invoice__["a" /* InvoicePage */];
        //...omitted for spinner
        this.showSplash = true; // show animation
        platform.ready().then(function () {
            platform.registerBackButtonAction(function () {
                var nav = _this.app.getActiveNavs()[0];
                var activeView = nav.getActive();
                // Checks if can go back before show up the alert
                if (activeView.name === 'ConfirmPage') {
                    _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_5__pages_invoice_invoice__["a" /* InvoicePage */]);
                }
                if (activeView.name === 'PaymentPage') {
                    _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_7__pages_confirm_confirm__["a" /* ConfirmPage */]);
                }
                if (activeView.name === 'DevicesModalPage') {
                    _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_6__pages_payment_payment__["a" /* PaymentPage */]);
                }
                if (activeView.name === 'InsertcardPage' || activeView.name === 'EnterPinPage') {
                    // this.nav.setRoot(PaymentPage);
                }
                if (activeView.name === 'ResultsPage') {
                    var confirm_1 = _this.alertCtrl.create({
                        title: 'Exit',
                        message: 'Are you sure you want to exit the app?',
                        buttons: [
                            {
                                text: 'Cancel',
                                handler: function () {
                                    return;
                                }
                            },
                            {
                                text: 'Confirm',
                                handler: function () {
                                    platform.exitApp();
                                }
                            }
                        ]
                    });
                    confirm_1.present();
                }
                else if (activeView.name === 'InvoicePage') {
                    var confirm_2 = _this.alertCtrl.create({
                        title: 'Exit',
                        message: 'Are you sure you want to exit the app?',
                        buttons: [
                            {
                                text: 'Cancel',
                                handler: function () {
                                    return;
                                }
                            },
                            {
                                text: 'Confirm',
                                handler: function () {
                                    platform.exitApp();
                                }
                            }
                        ]
                    });
                    confirm_2.present();
                }
            });
            statusBar.styleDefault();
            splashScreen.hide();
        });
        this.initializeApp();
        // this.data = { title:'', description:'', date:'', time:'' };
        // used for an example of ngFor and navigation
        this.pages = [
            { title: 'Payment', component: __WEBPACK_IMPORTED_MODULE_6__pages_payment_payment__["a" /* PaymentPage */] },
            { title: 'Invoice', component: __WEBPACK_IMPORTED_MODULE_5__pages_invoice_invoice__["a" /* InvoicePage */] },
        ];
    }
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
            Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_observable_timer__["timer"])(4000).subscribe(function () { return _this.showSplash = false; }); // hide animation after 4s
        });
    };
    MyApp.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.setRoot(page.component);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/home/austinwise1993/Documents/HealthPayment/src/app/app.html"*/'<ion-menu [content]="content">\n  <ion-header color="wise">\n    <ion-toolbar>\n      <ion-title>\n          <img src="../../assets/icon/icon.svg" alt="Smiley face" height="42" width="42" item-centre>\n      </ion-title>\n    </ion-toolbar>\n  </ion-header>\n\n  <ion-content>\n    <ion-list>\n      <button menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)">\n        {{p.title}}\n      </button>\n    </ion-list>\n\n  </ion-content>\n\n</ion-menu>\n\n<div *ngIf="showSplash" class="splash">\n  <div class="spinner">\n    <div class="dot1"></div>\n    <div class="dot2"></div>\n  </div>\n</div>\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>'/*ion-inline-end:"/home/austinwise1993/Documents/HealthPayment/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 33:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InvoicePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(85);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__confirm_confirm__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var InvoicePage = /** @class */ (function () {
    function InvoicePage(navCtrl, navParams, loadingCtrl, http, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.loadingCtrl = loadingCtrl;
        this.http = http;
        this.toastCtrl = toastCtrl;
    }
    InvoicePage.prototype.confirmPg = function () {
        var _this = this;
        if (this.posts == undefined) {
            var toast = this.toastCtrl.create({
                message: 'Please enter your invoice number',
                duration: 3000,
                position: 'top',
                cssClass: 'normalToast'
            });
            toast.present();
            return;
        }
        var loader = this.loadingCtrl.create({
            content: '<ion-spinner name="dots"></ion-spinner> Please wait...',
        });
        loader.present();
        //Http Request
        this.http.get('http://154.113.17.180:9011/api/v1/pos/invoicing/validateinvoicenumber?invoiceNumber='
            + this.posts).subscribe(function (res) {
            console.log('got here');
            console.log(res);
            localStorage.setItem('InvoiceId', res.InvoiceId);
            localStorage.setItem('InvoiceNumber', res.InvoiceNumber);
            localStorage.setItem('InvoiceStatus', res.InvoiceStatus);
            localStorage.setItem('Amount', res.Amount);
            localStorage.setItem('OutstandingAmount', res.OutstandingAmount);
            localStorage.setItem('PaidAmount', res.PaidAmount);
            localStorage.setItem('CurrencyCode', res.CurrencyCode ? res.CurrencyCode : '');
            localStorage.setItem('RevenueCode', res.RevenueCode ? res.RevenueCode : '');
            localStorage.setItem('CorporateCode', res.CorporateCode);
            localStorage.setItem('RevenueName', res.RevenueName);
            localStorage.setItem('InvoiceReference', res.InvoiceReference);
            localStorage.setItem('CorporateName', res.CorporateName);
            //Storing and setting the variable 
            localStorage.setItem('AdditionalInformation', JSON.stringify(res.AdditionalInformation));
            localStorage.setItem('InvoiceDetails', JSON.stringify(res.InvoiceDetails));
            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__confirm_confirm__["a" /* ConfirmPage */]);
            loader.dismiss();
        }, function (data) {
            loader.dismiss();
            console.log(data);
            var toast = _this.toastCtrl.create({
                message: data.error.Message,
                duration: 3000,
                position: 'top',
                cssClass: 'normalToast'
            });
            toast.present();
            console.log('hello');
        });
    };
    //Http Request end
    InvoicePage.prototype.ionViewDidEnter = function () {
        this.navBar.backButtonClick = function () {
        };
    };
    InvoicePage.prototype.ionViewDidLoad = function () {
        // console.log('ionViewDidLoad InvoicePage');
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('navbar'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Navbar */])
    ], InvoicePage.prototype, "navBar", void 0);
    InvoicePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-invoice',template:/*ion-inline-start:"/home/austinwise1993/Documents/HealthPayment/src/pages/invoice/invoice.html"*/'<ion-content #navbar padding class="bgImg">\n    \n    <div class="conEdit">\n       <h2>Health Pay</h2>\n        <p>To Proceed, Kindly input Invoice number</p>\n    </div>\n    \n    <span style="margin-left: 28px;">Enter Invoice number</span>\n  \n    <ion-item>\n        <ion-label stacked>Invoice number</ion-label>\n        <ion-input type="text" [(ngModel)]="posts"> </ion-input>\n    </ion-item>\n  \n    <button class="signin-btn" ion-button block color="blue" (click)="confirmPg()">\n        <span>Submit </span> \n    </button>\n</ion-content>\n  '/*ion-inline-end:"/home/austinwise1993/Documents/HealthPayment/src/pages/invoice/invoice.html"*/,
        }),
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ToastController */]])
    ], InvoicePage);
    return InvoicePage;
}());

//# sourceMappingURL=invoice.js.map

/***/ }),

/***/ 42:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PaymentPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__insertcard_insertcard__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__enter_pin_enter_pin__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__devices_modal_devices_modal__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__results_results__ = __webpack_require__(55);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






// Declarng interface end
var PaymentPage = /** @class */ (function () {
    //declaring a variable of type array
    function PaymentPage(modalCtrl, alertCtrl, navCtrl, navParams, loading) {
        this.modalCtrl = modalCtrl;
        this.alertCtrl = alertCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.loading = loading;
    }
    PaymentPage.prototype.insertCd = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__insertcard_insertcard__["a" /* InsertcardPage */]);
        // alert('it works');
    };
    PaymentPage.prototype.ionViewDidLoad = function () {
        this.InvoiceId = localStorage.getItem('InvoiceId');
        this.InvoiceNumber = localStorage.getItem('InvoiceNumber');
        this.InvoiceStatus = localStorage.getItem('InvoiceStatus');
        this.Amount = localStorage.getItem('Amount');
        this.OutstandingAmount = localStorage.getItem('OutstandingAmount');
        this.PaidAmount = localStorage.getItem('PaidAmount');
        this.CurrencyCode = localStorage.getItem('CurrencyCode');
        this.RevenueCode = localStorage.getItem('RevenueCode');
        this.RevenueName = localStorage.getItem('RevenueName');
        this.CorporateCode = localStorage.getItem('CorporateCode');
        this.CorporateName = localStorage.getItem('CorporateName');
        this.InvoiceReference = localStorage.getItem('InvoiceReference');
        // use a jsonpCallbackContext.parse() to parse your array and getItem() to get items,and also 
        // store in a localStorage for later use
        // Remember to setItem to where API is being called, i.e invoice.ts in our own case
        //Finally use *ngFor to bind and call the objetcs in this array, withing payment.html
        this.AdditionalInformation = JSON.parse(localStorage.getItem('AdditionalInformation'));
        this.InvoiceDetails = JSON.parse(localStorage.getItem('InvoiceDetails'));
    };
    PaymentPage.prototype.openModal = function (data) {
        var myModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__devices_modal_devices_modal__["a" /* DevicesModalPage */], { 'obj': data });
        myModal.present();
    };
    PaymentPage.prototype.connectToPinpadAndDownloadKeys = function () {
        var _this = this;
        var success = function (message) {
            if (message) {
                var myPopup = null;
                var deviceList = message.devices;
                _this.openModal(deviceList);
            }
            else {
                var alert_1 = _this.alertCtrl.create({
                    title: 'Important',
                    subTitle: 'Unable to complete operation, please try again later',
                    buttons: ['Dismiss']
                });
                alert_1.present();
            }
        };
        var failure = function (message) {
            var alert = _this.alertCtrl.create({
                title: 'Important',
                subTitle: 'Unable to complete operation, please try again later',
                buttons: ['Dismiss']
            });
            alert.present();
        };
        var loader = this.loading.create({
            content: '<ion-spinner name="dots"></ion-spinner><br />',
        });
        PaypadFacade.connection("", function (response) {
            console.log(response);
            var status = response.status;
            if (status) {
                if (status === "enabling_bluetooth") {
                    loader = _this.loading.create({
                        content: '<ion-spinner name="dots"></ion-spinner> <br />Please wait enabling bluetooth...',
                    });
                    loader.present();
                }
                else if (status == "connecting") {
                    loader.dismiss();
                    loader = _this.loading.create({
                        content: '<ion-spinner name="dots"></ion-spinner> <br /> Connecting to pinpad...',
                    });
                    loader.present();
                }
                else if (status === "connected" || status === "already_connected") {
                    var x = "2030ES03";
                    //console.log("i am connected from payment page")
                    loader.dismiss();
                    var selectedbank = "Esl";
                    var initState = localStorage.getItem("initialization");
                    console.log(initState);
                    if (initState === "false") {
                        PaypadFacade.reinitialization(x, true, selectedbank, function (result) {
                            console.log(result);
                            var status = result.status;
                            if (status) {
                                if (status === "downloading_keys") {
                                    loader = _this.loading.create({
                                        content: '<ion-spinner name="dots"></ion-spinner> <br />Please wait downloading keys...',
                                    });
                                    loader.present();
                                }
                                else if (status === "loading_keys_to_pinpad") {
                                    loader.dismiss();
                                    console.log('called payment from payment page');
                                    // alert("KEY DOWNLOAD SUCCESSFUL")
                                    var alert_2 = _this.alertCtrl.create({
                                        title: 'Status',
                                        subTitle: 'KEY DOWNLOAD SUCCESSFUL',
                                        buttons: ['Dismiss']
                                    });
                                    alert_2.present();
                                    localStorage.setItem("initialization", "true");
                                    _this.payment();
                                }
                                else if (status == "Already_initialised") {
                                    alert("already initialized");
                                }
                            }
                        }, function (error) {
                            console.log('Unable to download keys from connectToPinpad');
                            console.log(error);
                            loader.dismiss();
                            var alert = _this.alertCtrl.create({
                                title: 'Status',
                                subTitle: 'Unable to Download Keys',
                                buttons: ['Dismiss']
                            });
                            alert.present();
                        });
                        localStorage.setItem("initialization", "false");
                    }
                    else {
                        _this.payment();
                    }
                }
                else {
                    loader.dismiss();
                    // alert("This is else "+ status);
                    console.log("listing");
                    PaypadFacade.list(success, failure);
                }
            }
            else {
                // PaypadFacade.list(success,failure);//wise 
                _this.payment();
                console.log("not status");
                console.log(status);
                //reject("no response status from connection")
            }
        }, function (response) {
            loader.dismiss();
            PaypadFacade.list(success, failure);
        });
    };
    PaymentPage.prototype.payment = function () {
        var _this = this;
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__insertcard_insertcard__["a" /* InsertcardPage */]);
        var loader = this.loading.create({
            content: '<ion-spinner name="dots"></ion-spinner> <br />',
        });
        PaypadFacade.payment({
            "amount": localStorage.getItem('Amount'),
        }, function (data) {
            console.log(data);
            if (data.operation === "payment") {
                if (data.status === "processing") {
                    loader = _this.loading.create({
                        content: '<ion-spinner name="dots"></ion-spinner> <br />Processing...',
                    });
                    loader.present();
                }
                else if (data.status === "enterPin") {
                    loader.dismiss();
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__enter_pin_enter_pin__["a" /* EnterPinPage */]);
                    loader.dismiss();
                }
                else {
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__results_results__["a" /* ResultsPage */], { "obj": data });
                    loader.dismiss();
                }
            }
        }, function (error) {
            loader.dismiss();
            // this.navCtrl.push(InvoicePage)
            console.log(error);
        });
    };
    PaymentPage.prototype.doRefresh = function (refresher) {
        console.log('Started', refresher);
        setTimeout(function () {
            console.log('Async operation has ended');
            refresher.complete();
        }, 4000);
    };
    PaymentPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-payment',template:/*ion-inline-start:"/home/austinwise1993/Documents/HealthPayment/src/pages/payment/payment.html"*/'<ion-header no-border>\n  <ion-toolbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>\n      <img src="../../assets/icon/icon.svg" alt="Smiley face" height="42" width="42">\n    </ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="notify()">\n        <ion-icon name="notifications"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-toolbar>\n  <!-- <ion-toolbar></ion-toolbar>\n  <ion-toolbar></ion-toolbar> -->\n</ion-header>\n\n\n<ion-content padding class="mv-Top">\n\n  <ion-refresher (ionRefresh)="doRefresh($event)">\n    <ion-refresher-content\n      pullingIcon="arrow-dropdown"\n      pullingText="Pull Down."\n      refreshingSpinner="circles"\n      refreshingText="Loading...">\n    </ion-refresher-content>\n  </ion-refresher>\n\n  <!-- PATIENT DETAILS  -->\n  <ion-card class="welcome-card">\n    <ion-card-header class="head">\n        <ion-row>\n            <ion-col col-6 class="head_edit"><h2>PATIENT DETAILS</h2></ion-col>\n            <ion-col col-6>\n                <img src="../../assets/icon/user.svg" alt="hygieia" height="42" width="42">\n            </ion-col>\n        </ion-row>\n    </ion-card-header>\n    <ion-grid>\n        <ion-row class="row_edit">\n            <ion-col col-6 class="left"><span> Revenue Name  </span> </ion-col>\n            <ion-col col-6 class="right"><span><b>{{ RevenueName  }}</b></span></ion-col>\n        </ion-row>\n\n        <ion-row class="row_edit">\n            <ion-col col-6 class="left"><span> Invoice Id </span> </ion-col>\n            <ion-col col-6 class="right"><span><b>{{ InvoiceId }}</b></span></ion-col>\n          </ion-row>\n\n          <ion-row class="row_edit">\n              <ion-col col-6 class="left"><span> Invoice Number </span></ion-col>\n              <ion-col col-6 class="right"><span><b>{{ InvoiceNumber }}</b></span></ion-col>\n          </ion-row>\n              \n          <ion-row class="row_edit">\n            <ion-col col-6 class="left"><span> Invoice Status </span> </ion-col>\n            <ion-col col-6 class="right"><span>{{ InvoiceStatus }}</span></ion-col>\n          </ion-row>\n      \n          <ion-row class="row_edit">\n              <ion-col col-6 class="left"><span> Invoice Reference </span> </ion-col>\n              <ion-col col-6 class="right"><span><b>{{ InvoiceReference  }}</b></span></ion-col>\n          </ion-row>\n   </ion-grid>\n  </ion-card>\n\n<!-- AMOUNT DETAILS  -->\n<ion-card class="welcome-card">\n  <ion-card-header class="head">\n      <ion-row>\n          <ion-col col-6 class="head_edit"><h2>AMOUNT DETAILS</h2></ion-col>\n          <ion-col col-6>\n              <img src="../../assets/icon/coins.svg" alt="hygieia" height="42" width="42">\n          </ion-col>\n      </ion-row>\n  </ion-card-header>\n\n  <ion-grid>\n      \n          <ion-row class="row_edit">\n              <ion-col col-6 class="left"><span> Amount </span> </ion-col>\n              <ion-col col-6 class="right"><span><b>{{ Amount }}</b></span></ion-col>\n          </ion-row>\n\n          <ion-row class="row_edit">\n            <ion-col col-6 class="left"><span> Outstanding Amount </span> </ion-col>\n            <ion-col col-6 class="right"><span><b>{{ OutstandingAmount }}</b></span></ion-col>\n          </ion-row>\n\n          <ion-row class="row_edit">\n              <ion-col col-6 class="left"><span> Paid Amount </span> </ion-col>\n              <ion-col col-6 class="right"><span><b>{{ PaidAmount }}</b></span></ion-col>\n          </ion-row>\n\n          <ion-row class="row_edit">\n              <ion-col col-6 class="left"><span> Currency Code </span> </ion-col>\n              <ion-col col-6 class="right"><span><b>{{ CurrencyCode }}</b></span></ion-col>\n          </ion-row>\n\n          <ion-row class="row_edit">\n              <ion-col col-6 class="left"><span> Revenue Code </span> </ion-col>\n              <ion-col col-6 class="right"><span><b>{{ RevenueCode  }}</b></span></ion-col>\n          </ion-row>\n\n          <ion-list *ngFor="let Add of AdditionalInformation">\n\n            <ion-row class="row_edit">\n                <ion-col col-6 class="left"> <span >Name</span> </ion-col>\n                <ion-col col-6 class="right"><span >{{ Add.Name }}</span></ion-col>\n            </ion-row>\n            \n            <ion-row class="row_edit">\n              <ion-col col-6 class="left"><span> Value </span> </ion-col>\n              <ion-col col-6 class="right"><span><b>{{ Add.Value }}</b></span></ion-col>\n          </ion-row>\n            \n          </ion-list>\n      </ion-grid>\n\n    <button class="signin-btn" ion-button block color="blue" (click)="connectToPinpadAndDownloadKeys()\n      ">\n        <span>Make Payment  </span> \n    </button>\n</ion-card>\n\n<!-- INVOICE DETAILS -->\n<ion-card class="welcome-card">\n  <ion-card-header class="head">\n      <ion-row>\n          <ion-col col-6 class="head_edit"><h2>INVOICE DETAILS</h2></ion-col>\n          <ion-col col-6>\n              <img src="../../assets/icon/approve-invoice.svg" alt="hygieia" height="42" width="42">\n          </ion-col>\n      </ion-row>\n  </ion-card-header>\n    \n  <ion-grid>\n    \n      <ion-list class="liEdit" *ngFor="let item of InvoiceDetails">\n          <ion-row class="row_edit">\n              <ion-col col-6 class="left"> <span >ID</span> </ion-col>\n              <ion-col col-6 class="right"><span >{{ item.id }}</span></ion-col>\n           </ion-row>\n\n           <ion-row class="row_edit">\n              <ion-col col-6 class="left"> <span >Invoice ID</span> </ion-col>\n              <ion-col col-6 class="right"><span >{{ item.invoiceId }}</span></ion-col>\n           </ion-row>\n\n           <ion-row class="row_edit">\n              <ion-col col-6 class="left"> <span >Item Id</span> </ion-col>\n              <ion-col col-6 class="right"><span >{{ item.itemId }}</span></ion-col>\n           </ion-row>\n\n           <ion-row class="row_edit">\n              <ion-col col-6 class="left"> <span >Item Description</span> </ion-col>\n              <ion-col col-6 class="right"><span >{{ item.itemDescription }}</span></ion-col>\n           </ion-row>\n\n           <ion-row class="row_edit">\n              <ion-col col-6 class="left"> <span >Entry Time</span> </ion-col>\n              <ion-col col-6 class="right"><span >{{ item.entryTime }}</span></ion-col>\n           </ion-row>\n\n           <ion-row class="row_edit">\n              <ion-col col-6 class="left"> <span >Amount</span> </ion-col>\n              <ion-col col-6 class="right"><span >{{ item.amount }}</span></ion-col>\n           </ion-row>\n      </ion-list>\n\n  </ion-grid>\n\n</ion-card>\n\n<!-- Institution Details -->\n<ion-card class="welcome-card">\n  <ion-card-header class="head">\n      <ion-row>\n          <ion-col col-6 class="head_edit"><h2>INSTITUTION DETAILS</h2> </ion-col>\n          <ion-col col-6>\n              <img src="../../assets/icon/hospital.svg" alt="hygieia" height="42" width="42">\n          </ion-col>\n      </ion-row>\n  </ion-card-header>\n\n  <ion-grid>\n    <ion-row class="row_edit">\n        <ion-col col-6 class="left"><span> Corporate Name  </span> </ion-col>\n        <ion-col col-6 class="right"><span><b>{{ CorporateName }}</b></span></ion-col>\n    </ion-row>\n\n    <ion-row class="row_edit">\n      <ion-col col-6 class="left"><span> Corporate Code </span> </ion-col>\n      <ion-col col-6 class="right"><span><b>{{ CorporateCode }}</b></span></ion-col>\n    </ion-row>    \n  </ion-grid>\n</ion-card>\n\n</ion-content>\n'/*ion-inline-end:"/home/austinwise1993/Documents/HealthPayment/src/pages/payment/payment.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */]])
    ], PaymentPage);
    return PaymentPage;
}());

//# sourceMappingURL=payment.js.map

/***/ }),

/***/ 52:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConfirmPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__invoice_invoice__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__payment_payment__ = __webpack_require__(42);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ConfirmPage = /** @class */ (function () {
    function ConfirmPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ConfirmPage.prototype.pay = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__payment_payment__["a" /* PaymentPage */]);
        // alert('it works')
        // console.log(this.AdditionalInformation[0]['Value'], this.AdditionalInformation[0]['Name'])
    };
    ConfirmPage.prototype.invoicePg = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__invoice_invoice__["a" /* InvoicePage */]);
        // alert('it works')
    };
    ConfirmPage.prototype.ionViewDidLoad = function () {
        this.InvoiceId = localStorage.getItem('InvoiceId');
        this.InvoiceNumber = localStorage.getItem('InvoiceNumber');
        this.InvoiceStatus = localStorage.getItem('InvoiceStatus');
        this.Amount = localStorage.getItem('Amount');
        this.OutstandingAmount = localStorage.getItem('OutstandingAmount');
        this.PaidAmount = localStorage.getItem('PaidAmount');
        this.CurrencyCode = localStorage.getItem('CurrencyCode');
        this.RevenueCode = localStorage.getItem('RevenueCode');
        this.RevenueName = localStorage.getItem('RevenueName');
        this.CorporateCode = localStorage.getItem('CorporateCode');
        this.CorporateName = localStorage.getItem('CorporateName');
    };
    ConfirmPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-confirm',template:/*ion-inline-start:"/home/austinwise1993/Documents/HealthPayment/src/pages/confirm/confirm.html"*/'\n<ion-header no-border>\n  <ion-toolbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title><img src="../../assets/icon/icon.svg" alt="Smiley face" height="42" width="42">\n    </ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="notify()">\n        <ion-icon name="notifications"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-toolbar>\n  <!-- <ion-toolbar></ion-toolbar>\n  <ion-toolbar></ion-toolbar> -->\n</ion-header>\n\n<ion-content #navbar padding >\n    <!-- AMOUNT DETAILS  -->\n    <ion-card class="welcome-card">\n      <ion-card-header class="head">\n          <ion-row>\n              <ion-col col-6 class="head_edit"><h2>AMOUNT DETAILS</h2></ion-col>\n              <ion-col col-6>\n                  <img src="../../assets/icon/coins.svg" alt="hygieia" height="42" width="42">\n              </ion-col>\n          </ion-row>\n      </ion-card-header>\n\n      <ion-grid>         \n          <ion-row class="row_edit">\n              <ion-col col-6 class="left"><span> Amount </span> </ion-col>\n              <ion-col col-6 class="right"><span><b>{{ Amount }}</b></span></ion-col>\n          </ion-row>\n\n          <ion-row class="row_edit">\n            <ion-col col-6 class="left"><span> Outstanding Amount </span> </ion-col>\n            <ion-col col-6 class="right"><span><b>{{ OutstandingAmount }}</b></span></ion-col>\n          </ion-row>\n\n          <ion-row class="row_edit">\n              <ion-col col-6 class="left"><span> Paid Amount </span> </ion-col>\n              <ion-col col-6 class="right"><span><b>{{ PaidAmount }}</b></span></ion-col>\n          </ion-row>\n\n          <ion-row class="row_edit">\n              <ion-col col-6 class="left"><span> Currency Code </span> </ion-col>\n              <ion-col col-6 class="right" ><span><b>{{ CurrencyCode }}</b></span></ion-col>\n          </ion-row>\n\n          <ion-row class="row_edit">\n              <ion-col col-6 class="left"><span> Revenue Code </span> </ion-col>\n              <ion-col col-6 class="right"><span><b>{{ RevenueCode  }}</b></span></ion-col>\n          </ion-row> \n          \n        </ion-grid>\n    </ion-card>\n\n    <ion-grid>\n        <ion-row>\n          <ion-col col-6>\n              <button class="signin-btn " style="background-color: #2fa442" ion-button block color="blue" (click)="pay()">\n                  <span>Confirm</span> \n              </button>\n          </ion-col>\n\n          <ion-col col-6>\n              <button class="signin-btn" style="background-color:#e52a21" ion-button block color="danger" (click)="invoicePg()">\n                  <span>Cancel</span> \n              </button>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    \n</ion-content>\n'/*ion-inline-end:"/home/austinwise1993/Documents/HealthPayment/src/pages/confirm/confirm.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */]])
    ], ConfirmPage);
    return ConfirmPage;
}());

//# sourceMappingURL=confirm.js.map

/***/ }),

/***/ 53:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InsertcardPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


// import { PaymentPage } from '../payment/payment';
var InsertcardPage = /** @class */ (function () {
    // back() {
    //   this.navCtrl.push(PaymentPage);
    // }
    function InsertcardPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    // ionViewDidEnter() {
    //   this.navBar.backButtonClick = () => {
    //     here you can do wathever you want to replace the backbutton event
    //     this.navCtrl.push(PaymentPage);
    //   };
    // }
    InsertcardPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad InsertcardPage');
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('navbar'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Navbar */])
    ], InsertcardPage.prototype, "navBar", void 0);
    InsertcardPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-insertcard',template:/*ion-inline-start:"/home/austinwise1993/Documents/HealthPayment/src/pages/insertcard/insertcard.html"*/'<ion-header no-border>\n  <ion-toolbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title><img src="../../assets/icon/icon.svg" alt="Smiley face" height="42" width="42">\n    </ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="notify()">\n        <ion-icon name="notifications"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content padding>\n    <div class="payEdit">\n        <img src="../../assets/imgs/insertcard.svg" alt="hygieia" height="" width="">\n        <p >Please Insert Your Debit Card</p>\n    </div>\n</ion-content>\n'/*ion-inline-end:"/home/austinwise1993/Documents/HealthPayment/src/pages/insertcard/insertcard.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */]])
    ], InsertcardPage);
    return InsertcardPage;
}());

//# sourceMappingURL=insertcard.js.map

/***/ }),

/***/ 54:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EnterPinPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


// import { PaymentPage } from '../payment/payment';
var EnterPinPage = /** @class */ (function () {
    // back() {
    //   this.navCtrl.push(PaymentPage);
    // }
    function EnterPinPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    EnterPinPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad EnterPinPage');
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('navbar'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Navbar */])
    ], EnterPinPage.prototype, "navBar", void 0);
    EnterPinPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-enter-pin',template:/*ion-inline-start:"/home/austinwise1993/Documents/HealthPayment/src/pages/enter-pin/enter-pin.html"*/'<ion-header no-border>\n  <ion-toolbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title><img src="../../assets/icon/icon.svg" alt="Smiley face" height="42" width="42">\n    </ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="notify()">\n        <ion-icon name="notifications"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content padding>\n    <div class="payEdit">\n        <img src="../../assets/imgs/enterpin.svg" alt="hygieia" height="" width="">\n        <p >Please Enter Your Pin</p>\n    </div>\n</ion-content>\n'/*ion-inline-end:"/home/austinwise1993/Documents/HealthPayment/src/pages/enter-pin/enter-pin.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */]])
    ], EnterPinPage);
    return EnterPinPage;
}());

//# sourceMappingURL=enter-pin.js.map

/***/ }),

/***/ 55:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ResultsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(85);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__payment_payment__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_http_ngx__ = __webpack_require__(163);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ResultsPage = /** @class */ (function () {
    function ResultsPage(navCtrl, http, nativeHttp, navParams, viewCtrl) {
        this.navCtrl = navCtrl;
        this.http = http;
        this.nativeHttp = nativeHttp;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.resultImage = "";
    }
    ResultsPage.prototype.ionViewWillEnter = function () {
        this.viewCtrl.showBackButton(false);
    };
    ResultsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ResultsPage');
        var result = this.navParams.get('obj');
        var myDate = new Date().toISOString();
        document.getElementById("date").innerHTML = myDate;
        if (result.responsecode == "00") {
            this.resultImage = "assets/imgs/success.svg";
        }
        else {
            this.resultImage = "assets/imgs/failed.svg";
        }
        this.pan = result.pan;
        this.cardHolder = result.cardholder;
        this.stan = result.stan;
        this.rrn = result.rrn;
        this.responseCode = result.responsecode;
        this.responseMessage = result.responsemessage;
        // this.accountType = result.accounttype;
        this.tenderType = result.tendertype;
        var expiry = result.expiry;
        expiry.split("");
        var formattedDate = expiry[2] + "" + expiry[3] + "/" + expiry[0] + "" + expiry[1];
        this.expiry = formattedDate;
        var data = {
            Amount: localStorage.getItem('Amount'),
            CardScheme: result.tendertype,
            CurrencyCode: localStorage.getItem('CurrencyCode'),
            CustomerName: result.cardholder,
            MaskedPAN: result.pan,
            PaymentDate: myDate,
            Reference: localStorage.getItem('InvoiceNumber'),
            RetrievalReferenceNumber: result.rrn,
            StatusCode: result.responsecode,
            StatusDescription: result.responsemessage,
            TransactionReference: localStorage.getItem('InvoiceReference'),
            type: result.tendertype,
        };
        this.InvoiceNumber = localStorage.getItem('InvoiceNumber');
        console.log('Before request: ');
        try {
            this.nativeHttp.setDataSerializer('json');
            this.nativeHttp.post('http://154.113.17.180:9011/api/v1/pos/invoicing/invoicepayment?invoiceNumber=' + this.InvoiceNumber, data, {
                'ProviderID': '02',
                'Content-Type': 'application/json'
            }).then(function (res) {
                console.log('request success: ');
                console.log(res);
                alert(res);
            }).catch(function (e) {
                console.log('request fail: ');
                console.log(e);
            });
            console.log(this.nativeHttp.post);
        }
        catch (e) {
            console.log(e.message); // "null has no properties"
            console.log(e.name); // "TypeError"
            console.log(e.fileName); // "Scratchpad/1"
            console.log(e.lineNumber); // 2
            console.log(e.columnNumber); // 2
            console.log(e.stack);
            console.error('Request error: ', e);
        }
        console.log('After request: ');
    };
    ResultsPage.prototype.newTransaction = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__payment_payment__["a" /* PaymentPage */]);
    };
    ResultsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-results',template:/*ion-inline-start:"/home/austinwise1993/Documents/HealthPayment/src/pages/results/results.html"*/'\n<ion-header>\n  <ion-navbar>\n    <ion-title> Transaction Details </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content  padding>\n\n  <div class="resultImg">\n    <img [src]="resultImage"/>\n  </div>\n  <br>\n  <div>\n    <ion-list inset>\n      <ion-item><b>Response Code</b> : {{responseCode}}</ion-item>\n      <ion-item><b>Response Message</b> : {{responseMessage}}</ion-item>\n      <ion-item><b>PAN</b> : {{pan}}</ion-item>\n      <ion-item><b>STAN</b> : {{stan}}</ion-item>\n      <ion-item><b>Card Holder</b> : {{cardHolder}}</ion-item>\n      <ion-item><b>Expiry</b> : {{expiry}}</ion-item>\n      <!-- <ion-item><b>Account Type</b> : {{accountType}}</ion-item> -->\n      <ion-item><b>Tender Type</b> : {{tenderType}}</ion-item>\n      <ion-item><b>Date</b> : <span id="date"></span> </ion-item>\n    </ion-list>\n  </div>\n  <br>\n  <button ion-button full color="wise" (click)="newTransaction()"> NEW TRANSACTION</button>\n  <br>\n</ion-content>'/*ion-inline-end:"/home/austinwise1993/Documents/HealthPayment/src/pages/results/results.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_http_ngx__["a" /* HTTP */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */]])
    ], ResultsPage);
    return ResultsPage;
}());

//# sourceMappingURL=results.js.map

/***/ })

},[208]);
//# sourceMappingURL=main.js.map