webpackJsonp([0],{

/***/ 433:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelectAccountTypePageModule", function() { return SelectAccountTypePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__select_account_type__ = __webpack_require__(436);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SelectAccountTypePageModule = /** @class */ (function () {
    function SelectAccountTypePageModule() {
    }
    SelectAccountTypePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__select_account_type__["a" /* SelectAccountTypePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__select_account_type__["a" /* SelectAccountTypePage */]),
            ],
        })
    ], SelectAccountTypePageModule);
    return SelectAccountTypePageModule;
}());

//# sourceMappingURL=select-account-type.module.js.map

/***/ }),

/***/ 436:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SelectAccountTypePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SelectAccountTypePage = /** @class */ (function () {
    function SelectAccountTypePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    SelectAccountTypePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SelectAccountTypePage');
    };
    SelectAccountTypePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-select-account-type',template:/*ion-inline-start:"/home/austinwise1993/Documents/HealthPayment/src/pages/select-account-type/select-account-type.html"*/'\n<ion-header>\n  <ion-navbar>\n    <ion-title>Select Account Type</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n\n  <ion-list radio-group>\n     \n      <ion-item>\n        <ion-label>Savings</ion-label>\n        <ion-radio slot="start" [(ngModel)]="accountType" value="savings" checked></ion-radio>\n      </ion-item>\n  \n      <ion-item>\n        <ion-label>Current</ion-label>\n        <ion-radio slot="start" [(ngModel)]="accountType" value="current"></ion-radio>\n      </ion-item>\n  \n  </ion-list>\n\n    <button class="signin-btn" ion-button block color="blue" (click)="connectToPinpadAndDownloadKeys()\n    ">\n    <span>Make Payment  </span>\n    </button>\n</ion-content>\n'/*ion-inline-end:"/home/austinwise1993/Documents/HealthPayment/src/pages/select-account-type/select-account-type.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */]])
    ], SelectAccountTypePage);
    return SelectAccountTypePage;
}());

//# sourceMappingURL=select-account-type.js.map

/***/ })

});
//# sourceMappingURL=0.js.map