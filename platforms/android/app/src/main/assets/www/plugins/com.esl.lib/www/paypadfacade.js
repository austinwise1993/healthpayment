cordova.define("com.esl.lib.PaypadFacade", function(require, exports, module) {
module.exports = {
        list: function (successCallback, errorCallback) {
                cordova.exec(successCallback,
                        errorCallback,
                        "PaypadFacade",
                        "list",
                        []);
        },
        activation: function (activationCode, successCallback, errorCallback) {
                cordova.exec(successCallback,
                        errorCallback,
                        "PaypadFacade",
                        "activation",
                        [activationCode]);
        },
        connection: function (bluetoothAddress, successCallback, errorCallback) {
                cordova.exec(successCallback,
                        errorCallback,
                        "PaypadFacade",
                        "connection",
                        [bluetoothAddress]);
        },
        initialization: function (terminalId, doVas, bank, successCallback, errorCallback) {
                cordova.exec(successCallback,
                        errorCallback,
                        "PaypadFacade",
                        "initialization",
                        [terminalId, doVas, bank]);
        },
        reinitialization: function (terminalId,doVas,bank,successCallback, errorCallback) {
                cordova.exec(successCallback,
                        errorCallback,
                        "PaypadFacade",
                        "reinitialization",
                        [terminalId,doVas,bank]);
        },
        payment: function (params, successCallback, errorCallback) {
                cordova.exec(successCallback,
                        errorCallback,
                        "PaypadFacade",
                        "payment",
                        [params.amount, params.account]);
        },
        smspayment: function (params,successCallback, errorCallback) {
            cordova.exec(successCallback,
                    errorCallback,
                    "PaypadFacade",
                    "smspayment",
                    [params.amount, params.transactionId, params.permissions]);
        },
        vas: function (params, successCallback, errorCallback) {
                cordova.exec(successCallback,
                        errorCallback,
                        "PaypadFacade",
                        "vas",
                        [params.amount, params.account, params.stan, params.rrn]);
        },
        vasreversal: function (successCallback, errorCallback) {
                cordova.exec(successCallback,
                        errorCallback,
                        "PaypadFacade",
                        "vasreversal",
                        []);
        },
        network: function (successCallback, errorCallback) {
                cordova.exec(successCallback,
                        errorCallback,
                        "PaypadFacade",
                        "network",
                        []);
        },
        help: function (params, successCallback, errorCallback) {
                cordova.exec(successCallback,
                        errorCallback,
                        "PaypadFacade",
                        "help",
                        [params.email, params.name, params.body]);
        },
        sms: function (params, successCallback, errorCallback) {
                cordova.exec(successCallback,
                        errorCallback,
                        "PaypadFacade",
                        "sms",
                        [params.merchant, params.id, params.date, params.terminal, params.status, params.phone, params.amount]);
        },
        battery: function (successCallback, errorCallback) {
                cordova.exec(successCallback,
                        errorCallback,
                        "PaypadFacade",
                        "battery",
                        []);
        },
        connectprinter: function (bluetoothAddress, successCallback, errorCallback) {
                cordova.exec(successCallback,
                        errorCallback,
                        "PaypadFacade",
                        "connectprinter",
                        [bluetoothAddress]);
        },
        print: function (params, successCallback, errorCallback) {
                cordova.exec(successCallback,
                        errorCallback,
                        "PaypadFacade",
                        "print",
                        [params.header, params.body, params.footer, params.poweredby]);
        },
        startscan: function (successCallback, errorCallback) {
                cordova.exec(successCallback,
                        errorCallback,
                        "PaypadFacade",
                        "startscan",
                        []);
        },
        stopscan: function (successCallback, errorCallback) {
                cordova.exec(successCallback,
                        errorCallback,
                        "PaypadFacade",
                        "stopscan",
                        []);
        },
        changeport: function (params, successCallback, errorCallback) {
                cordova.exec(successCallback,
                        errorCallback,
                        "PaypadFacade",
                        "changeport",
                        [params.ip, params.port]);
        },
        showToast: function (message, successCallback, errorCallback) {
                cordova.exec(successCallback,
                        errorCallback,
                        "PaypadFacade",
                        "showToast",
                        [message]);
        },
        checkPermission : function(permissions,successCallback, errorCallback){
                 cordova.exec(successCallback,
                         errorCallback,
                         "PaypadFacade",
                         "checkPermission",
                         [permissions]);
        },
        registerOfflineTransactionHandler : function(successCallback, errorCallback){
                 cordova.exec(successCallback,
                         errorCallback,
                         "PaypadFacade",
                         "offlineTransactionHandler",
                         []);
        },
       ackOfflineResponse : function(params, successCallback, errorCallback){
           cordova.exec(successCallback,
                   errorCallback,
                   "PaypadFacade",
                   "ackOfflineResponse",
                   [params.transactionId]);
       }
};

});
