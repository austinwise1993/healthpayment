package exceptions;

public class TransactionCanceledException extends TransactionException {
    private static final long serialVersionUID = 1L;
    
    public TransactionCanceledException(String reason) {
        super(reason);
    }
}
