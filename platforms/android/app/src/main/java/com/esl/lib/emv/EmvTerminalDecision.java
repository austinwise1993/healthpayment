package emv;


public enum EmvTerminalDecision {
	EmvTerminalForceOnline,
	EmvTerminalForceDecline,
	EmvTerminalNoDecision

}
