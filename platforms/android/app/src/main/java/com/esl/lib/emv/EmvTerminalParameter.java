package emv;


public class EmvTerminalParameter {
	
	private boolean supportPSE;
	private boolean floorLimitCheck;
	private long floorLimit;
	private String tacDenial;
	private String tacOnline;
	private String tacDefault;
    private String acquierId;
	private String dDOL;
	private String tDOL;
	private String version;
	private String riskData;
	private String terminalCountryCode;
	private String terminalType;
	private String terminalId;
	private String terminalCapability;
	private String extendedTerminalCapabilites;
	private String refererCurrencyCode;
	private String merchantCatCode;
	private String merchantName;
	private String merchantid;
	public boolean isSupportPSE() {
		return supportPSE;
	}
	public void setSupportPSE(boolean supportPSE) {
		this.supportPSE = supportPSE;
	}
	public boolean isFloorLimitCheck() {
		return floorLimitCheck;
	}
	public void setFloorLimitCheck(boolean floorLimitCheck) {
		this.floorLimitCheck = floorLimitCheck;
	}
	public long getFloorLimit() {
		return floorLimit;
	}
	public void setFloorLimit(long floorLimit) {
		this.floorLimit = floorLimit;
	}
	public String getTacDenial() {
		return tacDenial;
	}
	public void setTacDenial(String tacDenial) {
		this.tacDenial = tacDenial;
	}
	public String getTacOnline() {
		return tacOnline;
	}
	public void setTacOnline(String tacOnline) {
		this.tacOnline = tacOnline;
	}
	public String getTacDefault() {
		return tacDefault;
	}
	public void setTacDefault(String tacDefault) {
		this.tacDefault = tacDefault;
	}
	public String getAcquierId() {
		return acquierId;
	}
	public void setAcquierId(String acquierId) {
		this.acquierId = acquierId;
	}
	public String getdDOL() {
		return dDOL;
	}
	public void setdDOL(String dDOL) {
		this.dDOL = dDOL;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getRiskData() {
		return riskData;
	}
	public void setRiskData(String riskData) {
		this.riskData = riskData;
	}
	public String getTerminalCountryCode() {
		return terminalCountryCode;
	}
	public void setTerminalCountryCode(String terminalCountryCode) {
		this.terminalCountryCode = terminalCountryCode;
	}
	public String getTerminalType() {
		return terminalType;
	}
	public void setTerminalType(String terminalType) {
		this.terminalType = terminalType;
	}
	public String getTerminalId() {
		return terminalId;
	}
	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}
	public String getTerminalCapability() {
		return terminalCapability;
	}
	public void setTerminalCapability(String terminalCapability) {
		this.terminalCapability = terminalCapability;
	}
	public String getExtendedTerminalCapabilites() {
		return extendedTerminalCapabilites;
	}
	public void setExtendedTerminalCapabilites(String extendedTerminalCapabilites) {
		this.extendedTerminalCapabilites = extendedTerminalCapabilites;
	}
	public String getRefererCurrencyCode() {
		return refererCurrencyCode;
	}
	public void setRefererCurrencyCode(String refererCurrencyCode) {
		this.refererCurrencyCode = refererCurrencyCode;
	}
	public String getMerchantCatCode() {
		return merchantCatCode;
	}
	public void setMerchantCatCode(String merchantCatCode) {
		this.merchantCatCode = merchantCatCode;
	}
	public String getMerchantName() {
		return merchantName;
	}
	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}
	public String getMerchantid() {
		return merchantid;
	}
	public void setMerchantid(String merchantid) {
		this.merchantid = merchantid;
	}
	public String gettDOL() {
		return tDOL;
	}
	public void settDOL(String tDOL) {
		this.tDOL = tDOL;
	}
	
	

}
