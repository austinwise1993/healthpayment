package extras;


import org.json.JSONObject;
import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HttpsURLConnection;
import java.net.HttpURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * Created by RASAQ PC on 29/09/15.
 */
public class Activation {

    public JSONObject doActivation(String regActivationCode) {

        JSONObject response = null;
        long result = 0;

        boolean status = false;
        try {
            // TrustManager tm = new X509TrustManager() {
            //     @Override
            //     public void checkClientTrusted(X509Certificate[] chain,
            //                                    String authType) throws CertificateException {
            //     }

            //     @Override
            //     public void checkServerTrusted(X509Certificate[] chain,
            //                                    String authType) throws CertificateException {
            //     }

            //     @Override
            //     public X509Certificate[] getAcceptedIssuers() {
            //         return null;
            //     }
            // };

            // SSLContext sc;

            // sc = SSLContext.getInstance("TLS");

            // sc.init(null, new TrustManager[]{tm}, null);

            // SSLSocketFactory ssf = sc.getSocketFactory();

            URL url = new URL(
                    "https://ibeta.paypad.com.ng/paypad/webapi/v2/activateterminal");

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            // conn.setSSLSocketFactory(ssf);

            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setUseCaches(false);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");

            JSONObject paramsJsonObj = new JSONObject();
            paramsJsonObj.put("activationCode", regActivationCode);

            byte[] postData = paramsJsonObj.toString().getBytes();
            OutputStream os = conn.getOutputStream();

            os.write(postData);

            os.flush();
            os.close();
            result = (long) conn.getResponseCode();
            System.out.print(result);
            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
            StringBuilder sb = new StringBuilder();
            String output;
            while ((output = br.readLine()) != null) {
                sb.append(output);
            }

            JSONObject jsonObject = new JSONObject(sb.toString());

            JSONArray array = jsonObject.getJSONArray("requestResponse");
            JSONObject object = array.getJSONObject(0);
            // object.getString("status-code");


            response = object;

        } catch (Exception e) {

            return response;

        }


        return response;
    }
}
