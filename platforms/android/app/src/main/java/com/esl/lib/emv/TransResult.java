package emv;


public enum TransResult {
	TransError,
	TransOnlineRequest,
	TransOfflineApprove,
	TransOfflineDecline,
	TransCancelled,
	TransTransactionfailed
}
