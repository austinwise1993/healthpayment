package extras;

import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;
import org.apache.cordova.*;
import com.esl.lib.PaypadFacade;

import models.messaging.nibss.Globals;

/**
 * Created by adeyemi on 4/17/15.
 */
public class BluetoothBroadCastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();

        if (BluetoothDevice.ACTION_ACL_CONNECTED.equals(action)) {
            //Do something if connected
            //Toast.makeText(context, "BT Connected", Toast.LENGTH_SHORT).show();
            BluetoothDevice device = intent
                    .getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

            if(device.getName().toLowerCase().startsWith("PP")){
                Globals.isPinpadConnected = true;
            }

        }
        else if (BluetoothDevice.ACTION_ACL_DISCONNECTED.equals(action)) {
            //Do something if disconnected
            // Toast.makeText(context, "BT Disconnected", Toast.LENGTH_SHORT).show();
            Globals.isPinpadConnected = false;
        }
    }

}
