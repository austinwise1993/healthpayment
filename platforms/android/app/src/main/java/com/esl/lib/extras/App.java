package extras;

/**
 * Created by RASAQ PC on 30/09/15.
 */
import android.app.Application;
import android.content.Context;

// public class App extends Application {

//     private static Application sApplication;

//     public static Application getApplication() {
//         return sApplication;
//     }

//     public static Context getContext() {
//         return getApplication().getApplicationContext();
//     }

//     @Override
//     public void onCreate() {
//         super.onCreate();
//         sApplication = this;
//     }
// }
public class App {

    // private static Application sApplication;

    public static Application getApplication() {
        try {
            Application app = (Application) Class.forName("android.app.AppGlobals")
            .getMethod("getInitialApplication").invoke(null, (Object[]) null);

            return app;
        }catch(Exception e){

        }
        
        return null;
    }

    public static Context getContext() {

        Application app = getApplication();
        
        if(app == null)
            return null;

        return getApplication().getApplicationContext();
    }

    // @Override
    // public void onCreate() {
    //     super.onCreate();
    //     sApplication = this;
    // }
}
