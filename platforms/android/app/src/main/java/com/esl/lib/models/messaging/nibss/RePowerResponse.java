package models.messaging.nibss;

import java.util.HashMap;
import java.util.Map;

public class RePowerResponse {

	public String field39;
	public Map<String, String> messages = new HashMap<String, String>();

	public void setField39(String field39) {
		this.field39 = field39;
	}

	public String getField39() {
		return field39;
	}

	public void returnMessage() {
		messages.put("00", "TRANSACTION SUCCESSFUL");
		messages.put("01", "REFER TO CARD ISSUER");
		messages.put("02", "REFER TO CARD ISSUER");
		messages.put("03", "INVALID MERCHANT");
		messages.put("04", "PICK-UP CARD");
		messages.put("05", "DO NOT HONOUR");
		messages.put("06", "ERROR");
		messages.put("07", "PICK-UP CARD");
		messages.put("08", "HONOUR WITH ID");
		messages.put("09", "REQUEST IN PROGRESS");
		messages.put("10", "APPROVED, PARTIAL");
		messages.put("11", "APPROVED, VIP");
		messages.put("12", "INVALID TRANSACTION");
		messages.put("13", "INVALID AMOUNT");
		messages.put("14", "INVALID CARD NUMBER");
		messages.put("15", "NO SUCH ISSUER");
		messages.put("16", "APPROVED UPDATE TRACK3");
		messages.put("17", "CUSTOMER CANCELLATION");
		messages.put("18", "CUSTOMER DISPUTE");
		messages.put("19", "RE-ENTER TRANSACTION");
		messages.put("20", "INVALID RESPONSE");
		messages.put("21", "NO ACTION TAKEN");
		messages.put("22", "SUSPECTED MALFUNCTION");
		messages.put("23", "UNACCEPTABLE FEE FOR TRANSACTION");
		messages.put("24", "FILE UPDATE NOT SUPPORTED");
		messages.put("25", "UNABLE TO LOCATE RECORD");
		messages.put("26", "DUPLICATE RECORD");
		messages.put("27", "FILE UPDATE; EDIT ERROR");
		messages.put("28", "FILE UPDATE; FILE LOCKED");
		messages.put("29", "FILE UPDATE FAILED");
		messages.put("30", "FORMAT ERROR");
		messages.put("31", "BANK NOT SUPPORTED");
		messages.put("32", "COMPLETED PARTIALLY");
		messages.put("33", "EXPIRED CARD; PICK-UP");
		messages.put("34", "SUSPECTED FRAUD; PICK-UP");
		messages.put("35", "CONTACT ACQUIRER; PICK-UP");
		messages.put("36", "RESTRICTED CARD; PICK-UP");
		messages.put("37", "CALL ACQUIRER SECURITY; PICK-UP");
		messages.put("38", "PIN TRIES EXCEEDED; PICK-UP");
		messages.put("39", "NO CREDIT ACCOUNT");
		messages.put("40", "FUNCTION NOT SUPPORTED");
		messages.put("41", "LOST CARD");
		messages.put("42", "NO UNIVERSAL ACCOUNT");
		messages.put("43", "STOLEN CARD");
		messages.put("44", "NO INVESTMENT ACCOUNT");
		messages.put("51", "INSUFFICIENT FUND");
		messages.put("52", "NO CURRENT ACCOUNT");
		messages.put("53", "NO SAVINGS ACCOUNT");
		messages.put("54", "CARD EXPIRED");
		messages.put("55", "INCORRECT PIN");
		messages.put("56", "NO CARD RECORD");
		messages.put("57", "TRANSACTION NOT PERMITTED");
		messages.put("58", "TRANSACTION NOT PERMITTED");
		messages.put("59", "SUSPECTED FRAUD");
		messages.put("60", "CONTACT ACQUIRER");
		messages.put("61", "EXCEEDS WITHDRAWAL LIMIT");
		messages.put("62", "RESTRICTED CARD");
		messages.put("63", "SECURITY VIOLATION");
		messages.put("64", "ORIGINAL AMOUNT INCORRECT");
		messages.put("65", "EXCEEDS WITHDRAWAL FREQ");
		messages.put("66", "CALL ACQUIRER SECURITY");
		messages.put("67", "HARD CAPTURE");
		messages.put("68", "RESPONSE RECEIVED TOO LATE");
		messages.put("75", "PIN TRY EXCEEDED");
		messages.put("77", "BANK INTERVENE");
		messages.put("78", "BANK INTERVENE");
		messages.put("90", "CUT-OFF IN PROGRESS");
		messages.put("91", "ISSUER OR SWITCH INOPERATIVE");
		messages.put("92", "ROUTING ERROR");
		messages.put("93", "LAW VIOLATION");
		messages.put("94", "DUPLICATE TRANSACTION");
		messages.put("95", "RECONCILE ERROR");
		messages.put("96", "  SYSTEM MALFUNCTION");
		messages.put("98", "EXCEEDS CASH LIMIT");
	}
}
