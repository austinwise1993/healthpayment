//
//  PurchaseResponse.h
//  MyBluetoothTest
//
//  Created by Emmanuel Adeyemi on 2/16/16.
//  Copyright © 2016 Emmanuel Adeyemi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PurchaseResponse : NSObject

@property(assign,nonatomic) NSString *field39;

@property(assign,nonatomic) NSString *responseMessage;

+(NSString *) getResponseString: (NSString *) value;

@end
