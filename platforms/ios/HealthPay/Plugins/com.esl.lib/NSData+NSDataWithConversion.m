//
//  NSData+NSDataWithConversion.m
//  MyBluetoothTest
//
//  Created by Emmanuel Adeyemi on 1/4/16.
//  Copyright © 2016 Emmanuel Adeyemi. All rights reserved.
//

#import "NSData+NSDataWithConversion.h"

@implementation NSData (NSDataWithConversion)

-(NSString *)hexadecimalString{
    
    const unsigned char *dataBuffer = (const unsigned char *)[self bytes];
    
    if (!dataBuffer)
        return [NSString string];
    
    NSUInteger          dataLength  = [self length];
    NSMutableString     *hexString  = [NSMutableString stringWithCapacity:(dataLength * 2)];
    
    for (int i = 0; i < dataLength; ++i)
        [hexString appendString:[NSString stringWithFormat:@"%02lx", (unsigned long)dataBuffer[i]]];
    
    return [NSString stringWithString:hexString];
}

@end
