//
//  EMVRequest.h
//  MyBluetoothTest
//
//  Created by Emmanuel Adeyemi on 1/13/16.
//  Copyright © 2016 Emmanuel Adeyemi. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef  NS_ENUM(NSUInteger, OnlinePinMode){
    DUKPT,
    MS
};

typedef NS_ENUM(NSUInteger,AccountType){
    DEFAULT = 00,
    SAVINGS = 01,
    CHECK = 02,
    CREDIT = 03,
    UNIVERSAL = 04,
    INVESTMENT,
    ELECTRONICPURSE,
    MORTGAGE,
    INSTALLMENT
};

typedef NS_ENUM(NSUInteger,EmvTransactionType){
    EMVGOOD = 00
};

typedef NS_ENUM(NSUInteger,EmvTerminalDecision) {
    EmvTerminalForceOnline,
    EmvTerminalForceDecline,
    EmvTerminalNoDecision
};

@interface EMVRequest : NSObject


@property(getter=getOtherAmount, setter=setOtherAmount:, atomic) NSUInteger otherAmount;
@property(getter=getMinorAmount, setter=setMinorAmount:, atomic) NSUInteger minorAmount;
@property(getter=getTransactionDate, setter=setTransactionDate:, assign) NSDate* transactionDate;

@property(getter=getCurrencyCode, setter=setCurrencyCode:, assign) NSString* currencyCode;
@property(getter=getTransactionCategoryCode, setter=setTransactionCategoryCode:, assign) NSString* transactionCategoryCode;
@property(getter=getTransactionCurrencyCode, setter=setTransactionCurrencyCode:, assign) NSString* transactionCurrencyCode;

@property(getter=getTransactionType, setter=setTransactionType:, assign) EmvTransactionType transactionType;
@property(getter=getOnlinePinMode, setter=setOnlinePinMode:, assign) OnlinePinMode pinMode;
@property(getter=getAccountType, setter=setAccountType:, assign) AccountType accountType;
@property(getter=getTerminalDecision, setter=setTerminalDecision:, assign) EmvTerminalDecision terminalDecision;


@end
