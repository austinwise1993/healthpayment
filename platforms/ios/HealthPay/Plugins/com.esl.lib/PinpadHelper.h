//
//  PinpadHelper.h
//  MyBluetoothTest
//
//  Created by Emmanuel Adeyemi on 11/11/15.
//  Copyright © 2015 Emmanuel Adeyemi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DTDevices.h"

@interface PinpadHelper : NSObject

+(void)initScreen:(DTDevices*) dtdevice;
+(void)enterPinASync:(DTDevices*) dtdevice
             amount:(NSString*) amount;
+(void)clearScreen:(DTDevices*) dtdevice;
//+(void)beepSuccess:(DTDevices*) dtdevice;
//+(void)beepFailure:(DTDevices*) dtdevice;
//+(void)beepAttention:(DTDevices*) dtdevice;
//+(void)showAborted:(DTDevices*) dtdevice;
+(void)showCanceled:(DTDevices*) dtdevice;
//+(void)showDenied:(DTDevices*) dtdevice;
+(void)showSuccessful:(DTDevices*) dtdevice;
+(void)showMessage:(DTDevices*) dtdevice
           message:(NSString*) message;
+(void)showValidPin:(DTDevices*) dtdevice;
+(void)showInvalidPin:(DTDevices*) dtdevice;
//+(void)showBusy:(DTDevices*) dtdevice;
+(void)removeCard:(DTDevices*) dtdevice;
////+(void)select:(DTDevices*) dtdevice
////      appList:(NSArray*) appList;
+(BOOL)isCardInserted:(DTDevices*) dtdevice;
+(int)presentCard:(DTDevices *)dtdev ;
+(int)presentCard : (DTDevices*) dtdev quit:(int *) quit;
+(void)showWaiting : (DTDevices*) dtdev;
+(void)disconnectPinpad : (DTDevices*) dtdev;
+(NSString*)getDeviceSerial:(DTDevices *)dtdev;


@end
