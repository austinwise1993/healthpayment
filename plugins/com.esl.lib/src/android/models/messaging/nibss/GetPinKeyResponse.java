package models.messaging.nibss;

import utils.CryptoUtil;
import utils.HexUtil;


public class GetPinKeyResponse {
	private byte[] clearPinKey;
	private String encryptedPinKey;
	

	public byte[] getClearPinKey() {
		return clearPinKey;
	}


	public String getEncryptedPinKey() {
		return encryptedPinKey;
	}


	public void setEncryptedPinKey(String encryptedPinKey) {
		this.encryptedPinKey = encryptedPinKey;
	}
	
	public void descryptPinKey(byte[] tmk) {
		byte[] pinKeyBytes = HexUtil.hexStringToByteArray(encryptedPinKey.replace(" ", ""));
		byte[] tmkBytes = tmk;
		
		this.clearPinKey = CryptoUtil.decrypt3DESECB(tmkBytes, pinKeyBytes);
	}
	
	
	
	
}
