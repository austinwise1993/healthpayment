package models.messaging.nibss;

public class CallHomeRequest extends _0800Request {
	private String processingCode;
	
	public String getProcessingCode() {
		return processingCode;
	}
	public void setProcessingCode(String processingCode) {
		this.processingCode = processingCode;
	}
}
