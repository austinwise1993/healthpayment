package models.messaging.nibss;

import utils.*;

public class GetSessionKeyResponse {
	
	private byte[] clearSessionKey;
	private String encryptedSessionKey;
	
	public String getEncryptedSessionKey() {
		return encryptedSessionKey;
	}
	
	public void setEncryptedSessionKey(String encryptedSessionKey) {
		this.encryptedSessionKey = encryptedSessionKey; 
	}
	
	public byte[] getClearSessionKey() {
		return clearSessionKey;
	}

	public void decryptSessionKey(byte[] tmk) {
		
		byte[] sessionKeyBytes =  HexUtil.hexStringToByteArray(encryptedSessionKey);
		byte[] tmkBytes = tmk;		
		byte[] clearSessionKeyBytes = CryptoUtil.decrypt3DESECB(tmkBytes, sessionKeyBytes);		
		this.clearSessionKey = clearSessionKeyBytes;
	}
	
	
	
}
