package models.messaging.nibss;

import java.io.IOException;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import extras.ReferenceList;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.http.conn.ConnectTimeoutException;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import models.messaging.nibss.NibssIsoProcessor;
import models.messaging.nibss.PurchaseRequest;
import models.messaging.nibss.PurchaseResponse;
import network.NibssSocketManager;
import utils.HexUtil;
import com.solab.iso8583.IsoMessage;
import com.solab.iso8583.IsoType;
import com.solab.iso8583.IsoValue;
import com.solab.iso8583.MessageFactory;
import android.content.SharedPreferences;
import com.esl.lib.PaypadFacade;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;
import java.net.URL;
import java.net.MalformedURLException;
import models.messaging.nibss.Globals;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;

public class NibssIsoProcessor {

	private static NibssSocketManager socketPluger;
	private static byte[] compareBytes = new byte[0];

	public NibssIsoProcessor() {

	}

	public static PurchaseResponse process(PurchaseRequest request,
			byte[] sessionKey) {

		increment();

		PurchaseResponse response = null;
		try {

			IsoMessage ismsg = new IsoMessage();
			ismsg.setType(0x200);
			IsoValue<String> field2 = new IsoValue<String>(IsoType.LLVAR,
					request.getPan());
			Log.i("pan", request.getPan());
			IsoValue<String> field3 = new IsoValue<String>(IsoType.NUMERIC,
					request.getProcessingCode(), 6);
			Log.i("pcode", request.getProcessingCode());
			IsoValue<String> field4 = new IsoValue<String>(IsoType.NUMERIC,
					request.getTransactionAmount(), 12);
			Log.i("amt", request.getTransactionAmount());
			IsoValue<String> field7 = new IsoValue<String>(IsoType.DATE10,
					request.getTransmissionDateTime());
			Log.i("dandt", request.getTransmissionDateTime());
			IsoValue<String> field11 = new IsoValue<String>(IsoType.NUMERIC,
					request.getStan(), 6);
			Log.i("stan", request.getStan());
			IsoValue<String> field12 = new IsoValue<String>(IsoType.TIME,
					request.getLocalTransactionTime());
			Log.i("time", request.getLocalTransactionTime());
			IsoValue<String> field13 = new IsoValue<String>(IsoType.DATE4,
					request.getLocalTransactionDate());
			Log.i("date", request.getLocalTransactionDate());
			IsoValue<String> field14 = new IsoValue<String>(IsoType.DATE4,
					request.getCardExpirationDate());
			Log.i("expdate", request.getCardExpirationDate());
			IsoValue<String> field18 = new IsoValue<String>(IsoType.ALPHA,
					request.getMerchantType(), 4);
			Log.i("mertype", request.getMerchantType());
			IsoValue<String> field22 = new IsoValue<String>(IsoType.ALPHA,
					request.getPosEntryMode(), 3);
			Log.i("posentrymode", request.getPosEntryMode());
			IsoValue<String> field23 = new IsoValue<String>(IsoType.ALPHA,
					request.getCardSequenceNumber(), 3);
			Log.i("seqcardno", request.getCardSequenceNumber());
			IsoValue<String> field25 = new IsoValue<String>(IsoType.ALPHA,
					request.getPosConditionCode(), 2);
			Log.i("poscondtncode", request.getPosConditionCode());
			IsoValue<String> field26 = new IsoValue<String>(IsoType.NUMERIC,
					request.getPosPinCaptureCode(), 2);
			Log.i("pospincapturecode", request.getPosPinCaptureCode());
			IsoValue<String> field28 = new IsoValue<String>(IsoType.ALPHA,
					request.getTransactionFee(), 9);
			Log.i("transactnfee", request.getTransactionFee());
			IsoValue<String> field32 = new IsoValue<String>(IsoType.LLVAR,
					request.getAcquiringInstIdCode());
			Log.i("acqinstcode", request.getAcquiringInstIdCode());
			IsoValue<String> field35 = new IsoValue<String>(IsoType.LLVAR,
					request.getTrack2Data());
			Log.i("track2", request.getTrack2Data());
			IsoValue<String> field37 = new IsoValue<String>(IsoType.ALPHA,
					request.getRetrievalReferenceNumber(), 12);
			Log.i("rrn", request.getRetrievalReferenceNumber());
			IsoValue<String> field40 = new IsoValue<String>(IsoType.NUMERIC,
					request.getServiceRestrictionCode(), 3);
			Log.i("src", request.getServiceRestrictionCode());
			IsoValue<String> field41 = new IsoValue<String>(IsoType.ALPHA,
					request.getTerminalId(), 8);
			Log.i("tid", request.getTerminalId());
			IsoValue<String> field42 = new IsoValue<String>(IsoType.ALPHA,
					request.getCardAcceptorIdCode(), 15);
			Log.i("cac", request.getCardAcceptorIdCode());
			IsoValue<String> field43 = new IsoValue<String>(IsoType.ALPHA,
					request.getCardAcceptorNameLocation(), 40);
			Log.i("canc", request.getCardAcceptorNameLocation());
			IsoValue<String> field49 = new IsoValue<String>(IsoType.NUMERIC,
					request.getCurrencyCode(), 3);
			Log.i("currcode", request.getCurrencyCode());
			IsoValue<String> field52 = new IsoValue<String>(IsoType.ALPHA,
					request.getPinData(), 16);
			IsoValue<String> field55 = new IsoValue<String>(IsoType.LLLVAR,
					request.getIccData());
			Log.i("icc", request.getIccData());
			IsoValue<String> field59 = new IsoValue<String>(IsoType.LLLVAR,
					request.getTransportData());
			Log.i("icc", request.getTransportData());
			IsoValue<String> field123 = new IsoValue<String>(IsoType.LLLVAR,
					request.getPosDataCode());
			Log.i("icc", request.getPosDataCode());
			IsoValue<String> field128 = new IsoValue<String>(IsoType.ALPHA,
					new String(new byte[] { 0x0 }), 64);

			ismsg.setField(2, field2);
			ismsg.setField(3, field3);
			ismsg.setField(4, field4);
			ismsg.setField(7, field7);
			ismsg.setField(11, field11);
			ismsg.setField(12, field12);
			ismsg.setField(13, field13);
			ismsg.setField(14, field14);
			ismsg.setField(18, field18);
			ismsg.setField(22, field22);
			ismsg.setField(23, field23);
			ismsg.setField(25, field25);
			ismsg.setField(26, field26);
			ismsg.setField(28, field28);
			ismsg.setField(32, field32);
			ismsg.setField(35, field35);
			ismsg.setField(37, field37);
			ismsg.setField(40, field40);
			ismsg.setField(41, field41);
			ismsg.setField(42, field42);
			ismsg.setField(43, field43);
			ismsg.setField(49, field49);

			if (request.getPinData() != null) {
				ismsg.setField(52, field52);
			}

			ismsg.setField(55, field55);
			ismsg.setField(59, field59);
			ismsg.setField(123, field123);
			ismsg.setField(128, field128);

			byte[] bites = ismsg.writeData();

			int length = bites.length;

			/* Hash generation */
			byte[] temp = new byte[length - 64];

			if (length >= 64) {
				System.arraycopy(bites, 0, temp, 0, length - 64);
			}

			String hashHex = NibssIsoProcessor.generateHash256Value(temp,
					sessionKey);

			// PaypadFacade.showStaticToastLong(hashHex);

			IsoValue<String> field128update = new IsoValue<String>(
					IsoType.ALPHA, hashHex, 64);

			ismsg.setField(128, field128update);

			byte[] rawMsg = ismsg.writeData();

			int lenght = rawMsg.length;
			byte[] bLenght = new byte[2];
			bLenght[1] = (byte) lenght;
			bLenght[0] = (byte) (lenght >> 8);

			byte[] toSend = concat(bLenght, rawMsg);

			Log.i("PAYPADSDK", new String(toSend));

			String strSend = new String(toSend);

			// PaypadFacade.showStaticToastLong(strSend+" \n "+strSend.length());

			TrustManager tm = new X509TrustManager() {
			@Override
	        public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
	        }
			@Override
	        public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
	        }
	     	@Override
            public X509Certificate[] getAcceptedIssuers() {
               return null;
            }
        };

        SSLContext sc = SSLContext.getInstance("TLS");

        sc.init(null, new TrustManager[] { tm }, null);


        SSLSocketFactory ssf = sc.getSocketFactory();
        SSLSocket s = (SSLSocket) ssf.createSocket(Globals.ipAddress, Globals.port);

        s.startHandshake();
        s.setSoTimeout(65*1000);

		// Socket s = new Socket(Globals.ipAddress, Globals.port);

            Globals.purchaseRequest = request;
            // Globals.TSK = sessionKey;

            if (s.isConnected()) {
                System.out.println("I am connected");
                socketPluger = new NibssSocketManager(s);
            }

			socketPluger.sendData(toSend);

			byte[] responseBytes = socketPluger.receive();

			String strRes = new String(responseBytes);

			// PaypadFacade.showStaticToastLong("Response "+strRes);

			Log.i("PAYPADSDKRESPONSE", new String(responseBytes));

			MessageFactory<IsoMessage> responseMessageFactory = new MessageFactory<IsoMessage>();
			responseMessageFactory.addMessageTemplate(ismsg);
			responseMessageFactory.setAssignDate(true);
			responseMessageFactory.setUseBinaryBitmap(false);
			responseMessageFactory.setUseBinaryMessages(false);
			responseMessageFactory.setEtx(-1);
			responseMessageFactory.setIgnoreLastMissingField(false);

			responseMessageFactory.setConfigPath("assets/www/isoconfig.xml");

			IsoMessage responseMessage = responseMessageFactory.parseMessage(
					responseBytes, 0);

			if (responseMessage != null) {

				// read response
				response = new PurchaseResponse();

				if (responseMessage.hasField(38)) {
					Globals.authCode = (String) responseMessage
							.getObjectValue(38);
				} else {
					Globals.authCode = "Not authenticated";

				}

				if (responseMessage.hasField(39)) {
					response.setField39((String) responseMessage
							.getObjectValue(39));
				}

				if (responseMessage.hasField(55)) {
					Globals.icc = (String) responseMessage.getObjectValue(55);
					Log.i("PAYPADSDKRESPONSEICC", Globals.icc);

					//getICCResponse(Globals.icc);
				}

			}

			Log.i("PAYPADSDK39", response.getField39());

			response.returnMessage();

			Globals.responseCode = response.getField39();
			Globals.appResponse = response.messages.get(response.getField39());
			

			getPinpadResponse();


		} catch (ConnectTimeoutException e) {

			Globals.responseCode = "XX";
			Globals.appResponse = "TRANSACTION NOT DONE ";
			Globals.returnMessage = "TRANSACTION\n NOT DONE ";

			Globals.authCode = "Not authenticated";

		} catch (ConnectException e) {

			Globals.responseCode = "XX";
			Globals.appResponse = "TRANSACTION NOT DONE ";
			Globals.returnMessage = "TRANSACTION\n NOT DONE ";

			Globals.authCode = "Not authenticated";

		}catch (SocketTimeoutException e) {

			reversal(request, sessionKey);
			response = new PurchaseResponse();
			//No Response and code is set in the reversal block
			//We set everythinh here whether failed or passed
			Globals.responseCode = "XX";
			Globals.appResponse = "TRANSACTION TIMED OUT ";
			Globals.returnMessage = "TRANSACTION\n TIMED OUT ";

			Globals.authCode = "Not authenticated";

		}catch (Exception e) {

			e.printStackTrace();
			String error = "";
            for(StackTraceElement element : e.getStackTrace()){
                error += element.toString() + "";
            }

			if (Globals.responseCode.equalsIgnoreCase("00")){

			}else{

				Globals.responseCode = "XX";
				Globals.appResponse = "ERROR OCCURRED";
				Globals.returnMessage = "ERROR OCCURRED";
       
				Globals.authCode = "Not authenticated";
			}

			logToWebHook(e);
		}

		return response;
	}

	public static CashBackResponse process(CashBackRequest request,
			byte[] sessionKey) {

		CashBackResponse response = null;
		try {

			IsoMessage ismsg = new IsoMessage();
			ismsg.setType(0x200);
			IsoValue<String> field2 = new IsoValue<String>(IsoType.LLVAR,
					request.getPan());
			Log.i("pan", request.getPan());
			IsoValue<String> field3 = new IsoValue<String>(IsoType.NUMERIC,
					request.getProcessingCode(), 6);
			Log.i("pcode", request.getProcessingCode());
			IsoValue<String> field4 = new IsoValue<String>(IsoType.NUMERIC,
					request.getTransactionAmount(), 12);
			Log.i("amt", request.getTransactionAmount());
			IsoValue<String> field7 = new IsoValue<String>(IsoType.DATE10,
					request.getTransmissionDateTime());
			Log.i("dandt", request.getTransmissionDateTime());
			IsoValue<String> field11 = new IsoValue<String>(IsoType.NUMERIC,
					request.getStan(), 6);
			Log.i("stan", request.getStan());
			IsoValue<String> field12 = new IsoValue<String>(IsoType.TIME,
					request.getLocalTransactionTime());
			Log.i("time", request.getLocalTransactionTime());
			IsoValue<String> field13 = new IsoValue<String>(IsoType.DATE4,
					request.getLocalTransactionDate());
			Log.i("date", request.getLocalTransactionDate());
			IsoValue<String> field14 = new IsoValue<String>(IsoType.DATE4,
					request.getCardExpirationDate());
			Log.i("expdate", request.getCardExpirationDate());
			IsoValue<String> field18 = new IsoValue<String>(IsoType.ALPHA,
					request.getMerchantType(), 4);
			Log.i("mertype", request.getMerchantType());
			IsoValue<String> field22 = new IsoValue<String>(IsoType.ALPHA,
					request.getPosEntryMode(), 3);
			Log.i("posentrymode", request.getPosEntryMode());
			IsoValue<String> field23 = new IsoValue<String>(IsoType.ALPHA,
					request.getCardSequenceNumber(), 3);
			Log.i("seqcardno", request.getCardSequenceNumber());
			IsoValue<String> field25 = new IsoValue<String>(IsoType.ALPHA,
					request.getPosConditionCode(), 2);
			Log.i("poscondtncode", request.getPosConditionCode());
			IsoValue<String> field26 = new IsoValue<String>(IsoType.NUMERIC,
					request.getPosPinCaptureCode(), 2);
			Log.i("pospincapturecode", request.getPosPinCaptureCode());
			IsoValue<String> field28 = new IsoValue<String>(IsoType.ALPHA,
					request.getTransactionFee(), 9);
			Log.i("transactnfee", request.getTransactionFee());
			IsoValue<String> field32 = new IsoValue<String>(IsoType.LLVAR,
					request.getAcquiringInstIdCode());
			Log.i("acqinstcode", request.getAcquiringInstIdCode());
			IsoValue<String> field35 = new IsoValue<String>(IsoType.LLVAR,
					request.getTrack2Data());
			Log.i("track2", request.getTrack2Data());
			IsoValue<String> field37 = new IsoValue<String>(IsoType.ALPHA,
					request.getRetrievalReferenceNumber(), 12);
			Log.i("rrn", request.getRetrievalReferenceNumber());
			IsoValue<String> field40 = new IsoValue<String>(IsoType.NUMERIC,
					request.getServiceRestrictionCode(), 3);
			Log.i("src", request.getServiceRestrictionCode());
			IsoValue<String> field41 = new IsoValue<String>(IsoType.ALPHA,
					request.getTerminalId(), 8);
			Log.i("tid", request.getTerminalId());
			IsoValue<String> field42 = new IsoValue<String>(IsoType.ALPHA,
					request.getCardAcceptorIdCode(), 15);
			Log.i("cac", request.getCardAcceptorIdCode());
			String loc = request.getCardAcceptorNameLocation();
			IsoValue<String> field43 = new IsoValue<String>(IsoType.ALPHA,
					request.getCardAcceptorNameLocation(), 40);
			Log.i("canc", request.getCardAcceptorNameLocation());
			IsoValue<String> field49 = new IsoValue<String>(IsoType.NUMERIC,
					request.getCurrencyCode(), 3);
			Log.i("currcode", request.getCurrencyCode());
			IsoValue<String> field52 = new IsoValue<String>(IsoType.ALPHA,
					request.getPinData(), 16);
			IsoValue<String> field54 = new IsoValue<String>(IsoType.LLLVAR,
					request.getAdditionalAmounts());
			Log.i("other amount", field54.toString());
			IsoValue<String> field55 = new IsoValue<String>(IsoType.LLLVAR,
					request.getIccData());
			Log.i("icc", request.getIccData());
			IsoValue<String> field59 = new IsoValue<String>(IsoType.LLLVAR,
					request.getTransportData());
			Log.i("icc", request.getTransportData());

			IsoValue<String> field123 = new IsoValue<String>(IsoType.LLLVAR,
					request.getPosDataCode());
			Log.i("icc", request.getPosDataCode());
			IsoValue<String> field128 = new IsoValue<String>(IsoType.ALPHA,
					new String(new byte[] { 0x0 }), 64);

			ismsg.setField(2, field2);
			ismsg.setField(3, field3);
			ismsg.setField(4, field4);
			ismsg.setField(7, field7);
			ismsg.setField(11, field11);
			ismsg.setField(12, field12);
			ismsg.setField(13, field13);
			ismsg.setField(14, field14);
			ismsg.setField(18, field18);
			ismsg.setField(22, field22);
			ismsg.setField(23, field23);
			ismsg.setField(25, field25);
			ismsg.setField(26, field26);
			ismsg.setField(28, field28);
			ismsg.setField(32, field32);
			ismsg.setField(35, field35);
			ismsg.setField(37, field37);
			Log.i("PAYPADSDK", field37.toString());
			ismsg.setField(40, field40);
			ismsg.setField(41, field41);
			ismsg.setField(42, field42);
			ismsg.setField(43, field43);
			ismsg.setField(49, field49);

			if (request.getPinData() != null) {
				ismsg.setField(52, field52);
				Log.i("pin", request.getPinData());
			}

			ismsg.setField(54, field54);
//			ismsg.setField(55, field55);
//			ismsg.setField(59, field59);

			ismsg.setField(123, field123);
			ismsg.setField(128, field128);

			byte[] bites = ismsg.writeData();

			int length = bites.length;

			/* Hash generation */
			byte[] temp = new byte[length - 64];

			if (length >= 64) {
				System.arraycopy(bites, 0, temp, 0, length - 64);
			}

			String hashHex = NibssIsoProcessor.generateHash256Value(temp,
					sessionKey);

			IsoValue<String> field128update = new IsoValue<String>(
					IsoType.ALPHA, hashHex, 64);

			// Update field 128
			ismsg.setField(128, field128update);

			byte[] rawMsg = ismsg.writeData();

			int lenght = rawMsg.length;
			byte[] bLenght = new byte[2];
			bLenght[1] = (byte) lenght; // (lenght & 0xFF);
			bLenght[0] = (byte) (lenght >> 8); // ((lenght >> 8) & 0xFF);
			byte[] toSend = concat(bLenght, rawMsg);

			Log.i("PAYPADSDK", new String(toSend));

			TrustManager tm = new X509TrustManager() {
				@Override
	            public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
	            }
				@Override
	            public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
	            }
				@Override
	            public X509Certificate[] getAcceptedIssuers() {
	                return null;
	            }
	        };

	        SSLContext sc = SSLContext.getInstance("SSL");

            sc.init(null, new TrustManager[] { tm }, null);

            SSLSocketFactory ssf = sc.getSocketFactory();
            SSLSocket s = (SSLSocket) ssf.createSocket(Globals.ipAddress, Globals.port);
            s.startHandshake();

            s.setSoTimeout(70*1000);

            Globals.cashbackRequest = request;
            // Globals.TSK = sessionKey;

            if (s.isConnected()) {
                System.out.println("I am connected");
                socketPluger = new NibssSocketManager(s);

            }

			socketPluger.sendData(toSend);

			byte[] responseBytes = socketPluger.receive();

			if (responseBytes.equals(null)|| Arrays.equals(responseBytes, compareBytes)) {
				response = new CashBackResponse();
				return response;
			}
			Log.i("PAYPADSDKRESPONSE", new String(responseBytes));

			MessageFactory<IsoMessage> responseMessageFactory = new MessageFactory<IsoMessage>();
			responseMessageFactory.addMessageTemplate(ismsg);
			responseMessageFactory.setAssignDate(true);
			responseMessageFactory.setUseBinaryBitmap(false);
			responseMessageFactory.setUseBinaryMessages(false);
			responseMessageFactory.setEtx(-1);
			responseMessageFactory.setIgnoreLastMissingField(false);

			responseMessageFactory.setConfigPath("assets/www/isoconfig.xml");

			IsoMessage responseMessage = responseMessageFactory.parseMessage(
					responseBytes, 0);

			if (responseMessage != null) {

				// read response
				response = new CashBackResponse();
				if (responseMessage.hasField(38)) {
					Globals.authCode = (String) responseMessage
							.getObjectValue(38);
				} else {
					Globals.authCode = "Not authenticated";

				}

				if (responseMessage.hasField(39)) {
					response.setField39((String) responseMessage
							.getObjectValue(39));
				}

				if (responseMessage.hasField(55)) {
					Globals.icc = (String) responseMessage.getObjectValue(55);
					Log.i("PAYPADSDKRESPONSEICC", Globals.icc);

					getICCResponse(Globals.icc);
				}

			}

			Log.i("PAYPADSDK39", response.getField39());

			response.returnMessage();

			Globals.responseCode = response.getField39();
			Globals.appResponse = response.messages.get(response.getField39());

			getPinpadResponse();

		} catch (SocketTimeoutException e) {

			reversal(request, sessionKey);
			response = new CashBackResponse();
			return response;

		} catch (Exception e) {

			e.printStackTrace();
			Globals.responseCode = "XX";
			Globals.appResponse = "UNCOMPLETED TRANSACTION ";
			Globals.returnMessage = "UNCOMPLETED\n TRANSACTION";

		}

		return response;
	}

	public static CashAdvanceResponse process(CashAdvanceRequest request,
			byte[] sessionKey) {

		CashAdvanceResponse response = null;
		try {

			IsoMessage ismsg = new IsoMessage();
			ismsg.setType(0x200);
			IsoValue<String> field2 = new IsoValue<String>(IsoType.LLVAR,
					request.getPan());
			Log.i("pan", request.getPan());
			IsoValue<String> field3 = new IsoValue<String>(IsoType.NUMERIC,
					request.getProcessingCode(), 6);
			Log.i("pcode", request.getProcessingCode());
			IsoValue<String> field4 = new IsoValue<String>(IsoType.NUMERIC,
				request.getTransactionAmount(), 12);
			Log.i("amt", request.getTransactionAmount());
			IsoValue<String> field7 = new IsoValue<String>(IsoType.DATE10,
					request.getTransmissionDateTime());
			Log.i("dandt", request.getTransmissionDateTime());
			IsoValue<String> field11 = new IsoValue<String>(IsoType.NUMERIC,
					request.getStan(), 6);
			Log.i("stan", request.getStan());
			IsoValue<String> field12 = new IsoValue<String>(IsoType.TIME,
					request.getLocalTransactionTime());
			Log.i("time", request.getLocalTransactionTime());
			IsoValue<String> field13 = new IsoValue<String>(IsoType.DATE4,
					request.getLocalTransactionDate());
			Log.i("date", request.getLocalTransactionDate());
			IsoValue<String> field14 = new IsoValue<String>(IsoType.DATE4,
					request.getCardExpirationDate());
			Log.i("expdate", request.getCardExpirationDate());
			IsoValue<String> field18 = new IsoValue<String>(IsoType.ALPHA,
					request.getMerchantType(), 4);
			Log.i("mertype", request.getMerchantType());
			IsoValue<String> field22 = new IsoValue<String>(IsoType.ALPHA,
					request.getPosEntryMode(), 3);
			Log.i("posentrymode", request.getPosEntryMode());
			IsoValue<String> field23 = new IsoValue<String>(IsoType.ALPHA,
					request.getCardSequenceNumber(), 3);
			Log.i("seqcardno", request.getCardSequenceNumber());
			IsoValue<String> field25 = new IsoValue<String>(IsoType.ALPHA,
					request.getPosConditionCode(), 2);
			Log.i("poscondtncode", request.getPosConditionCode());
			IsoValue<String> field26 = new IsoValue<String>(IsoType.NUMERIC,
					request.getPosPinCaptureCode(), 2);
			Log.i("pospincapturecode", request.getPosPinCaptureCode());
			IsoValue<String> field28 = new IsoValue<String>(IsoType.ALPHA,
					request.getTransactionFee(), 9);
			Log.i("transactnfee", request.getTransactionFee());
			IsoValue<String> field32 = new IsoValue<String>(IsoType.LLVAR,
					request.getAcquiringInstIdCode());
			Log.i("acqinstcode", request.getAcquiringInstIdCode());
			IsoValue<String> field35 = new IsoValue<String>(IsoType.LLVAR,
					request.getTrack2Data());
			Log.i("track2", request.getTrack2Data());
			IsoValue<String> field37 = new IsoValue<String>(IsoType.ALPHA,
					request.getRetrievalReferenceNumber(), 12);
			Log.i("rrn", request.getRetrievalReferenceNumber());
			IsoValue<String> field40 = new IsoValue<String>(IsoType.NUMERIC,
					request.getServiceRestrictionCode(), 3);
			Log.i("src", request.getServiceRestrictionCode());
			IsoValue<String> field41 = new IsoValue<String>(IsoType.ALPHA,
					request.getTerminalId(), 8);
			Log.i("tid", request.getTerminalId());
			IsoValue<String> field42 = new IsoValue<String>(IsoType.ALPHA,
					request.getCardAcceptorIdCode(), 15);
			Log.i("cac", request.getCardAcceptorIdCode());
			String loc = request.getCardAcceptorNameLocation();
			IsoValue<String> field43 = new IsoValue<String>(IsoType.ALPHA,
					request.getCardAcceptorNameLocation(), 40);
			Log.i("canc", request.getCardAcceptorNameLocation());
			IsoValue<String> field49 = new IsoValue<String>(IsoType.NUMERIC,
					request.getCurrencyCode(), 3);
			Log.i("currcode", request.getCurrencyCode());
			IsoValue<String> field52 = new IsoValue<String>(IsoType.ALPHA,
					request.getPinData(), 16);
			IsoValue<String> field54 = new IsoValue<String>(IsoType.LLLVAR,
					request.getAdditionalAmounts());
			Log.i("other amount", field54.getValue());
			IsoValue<String> field55 = new IsoValue<String>(IsoType.LLLVAR,
					request.getIccData());
			Log.i("icc", request.getIccData());
			IsoValue<String> field59 = new IsoValue<String>(IsoType.LLLVAR,
					request.getTransportData());
			Log.i("icc", request.getTransportData());

			IsoValue<String> field123 = new IsoValue<String>(IsoType.LLLVAR,
					request.getPosDataCode());
			Log.i("icc", request.getPosDataCode());
			IsoValue<String> field128 = new IsoValue<String>(IsoType.ALPHA,
					new String(new byte[] { 0x0 }), 64);

			ismsg.setField(2, field2);
			ismsg.setField(3, field3);
			ismsg.setField(4, field4);
			ismsg.setField(7, field7);
			ismsg.setField(11, field11);
			ismsg.setField(12, field12);
			ismsg.setField(13, field13);
			ismsg.setField(14, field14);
			ismsg.setField(18, field18);
			ismsg.setField(22, field22);
			ismsg.setField(23, field23);
			ismsg.setField(25, field25);
			ismsg.setField(26, field26);
			ismsg.setField(28, field28);
			ismsg.setField(32, field32);
			ismsg.setField(35, field35);
			ismsg.setField(37, field37);
			Log.i("PAYPADSDK", field37.toString());
			ismsg.setField(40, field40);
			ismsg.setField(41, field41);
			ismsg.setField(42, field42);
			ismsg.setField(43, field43);
			ismsg.setField(49, field49);

			if (request.getPinData() != null) {
				ismsg.setField(52, field52);
				Log.i("pin", request.getPinData());
			}

			//ismsg.setField(54, field54);
			ismsg.setField(55, field55);
			//ismsg.setField(59, field59);

			ismsg.setField(123, field123);
			ismsg.setField(128, field128);

			byte[] bites = ismsg.writeData();

			int length = bites.length;

			/* Hash generation */
			byte[] temp = new byte[length - 64];

			if (length >= 64) {
				System.arraycopy(bites, 0, temp, 0, length - 64);
			}

			String hashHex = NibssIsoProcessor.generateHash256Value(temp,
					sessionKey);

			IsoValue<String> field128update = new IsoValue<String>(
					IsoType.ALPHA, hashHex, 64);

			// Update field 128
			ismsg.setField(128, field128update);

			byte[] rawMsg = ismsg.writeData();

			int lenght = rawMsg.length;
			byte[] bLenght = new byte[2];
			bLenght[1] = (byte) lenght; // (lenght & 0xFF);
			bLenght[0] = (byte) (lenght >> 8); // ((lenght >> 8) & 0xFF);
			byte[] toSend = concat(bLenght, rawMsg);

			Log.i("PAYPADSDK", new String(toSend));

			TrustManager tm = new X509TrustManager() {
				@Override
	            public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
	            }
				@Override
	            public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
	            }
				@Override
	            public X509Certificate[] getAcceptedIssuers() {
	                return null;
	            }
	        };

	        SSLContext sc = SSLContext.getInstance("SSL");

            sc.init(null, new TrustManager[] { tm }, null);

            SSLSocketFactory ssf = sc.getSocketFactory();
            SSLSocket s = (SSLSocket) ssf.createSocket(Globals.ipAddress, Globals.port);
            s.startHandshake();

            s.setSoTimeout(70*1000);

            Globals.cashadvanceRequest = request;
            // Globals.TSK = sessionKey;

            if (s.isConnected()) {
                System.out.println("I am connected");
                socketPluger = new NibssSocketManager(s);

            }

			socketPluger.sendData(toSend);

			byte[] responseBytes = socketPluger.receive();

			if (responseBytes.equals(null)|| Arrays.equals(responseBytes, compareBytes)) {
				response = new CashAdvanceResponse();
				return response;
			}
			Log.i("PAYPADSDKRESPONSE", new String(responseBytes));

			MessageFactory<IsoMessage> responseMessageFactory = new MessageFactory<IsoMessage>();
			responseMessageFactory.addMessageTemplate(ismsg);
			responseMessageFactory.setAssignDate(true);
			responseMessageFactory.setUseBinaryBitmap(false);
			responseMessageFactory.setUseBinaryMessages(false);
			responseMessageFactory.setEtx(-1);
			responseMessageFactory.setIgnoreLastMissingField(false);

			responseMessageFactory.setConfigPath("assets/isoconfig.xml");

			IsoMessage responseMessage = responseMessageFactory.parseMessage(
					responseBytes, 0);

			if (responseMessage != null) {

				// read response
				response = new CashAdvanceResponse();
				if (responseMessage.hasField(38)) {
					Globals.authCode = (String) responseMessage
							.getObjectValue(38);
				} else {
					Globals.authCode = "Not authenticated";

				}

				if (responseMessage.hasField(39)) {
					response.setField39((String) responseMessage
							.getObjectValue(39));
				}

				if (responseMessage.hasField(55)) {
					Globals.icc = (String) responseMessage.getObjectValue(55);
					Log.i("PAYPADSDKRESPONSEICC", Globals.icc);

					getICCResponse(Globals.icc);
				}

			}

			Log.i("PAYPADSDK39", response.getField39());

			response.returnMessage();

			Globals.responseCode = response.getField39();
			Globals.appResponse = response.messages.get(response.getField39());

			getPinpadResponse();

		} catch (SocketTimeoutException e) {

			reversal(request, sessionKey);
			response = new CashAdvanceResponse();
			return response;

		} catch (Exception e) {

			e.printStackTrace();
			Globals.responseCode = "XX";
			Globals.appResponse = "UNCOMPLETED TRANSACTION";
			Globals.returnMessage = "UNCOMPLETED\n TRANSACTION";

		}

		return response;
	}

	public static BalanceResponse process(BalanceRequest request,
			byte[] sessionKey) {

		BalanceResponse response = null;
		try {

			IsoMessage ismsg = new IsoMessage();
			ismsg.setType(0x100);
			IsoValue<String> field2 = new IsoValue<String>(IsoType.LLVAR,
					request.getPan());
			Log.i("pan", request.getPan());
			IsoValue<String> field3 = new IsoValue<String>(IsoType.NUMERIC,
					request.getProcessingCode(), 6);
			Log.i("pcode", request.getProcessingCode());
			IsoValue<String> field4 = new IsoValue<String>(IsoType.NUMERIC,
					request.getTransactionAmount(), 12);
			Log.i("amt", request.getTransactionAmount());
			IsoValue<String> field7 = new IsoValue<String>(IsoType.DATE10,
					request.getTransmissionDateTime());
			Log.i("dandt", request.getTransmissionDateTime());
			IsoValue<String> field11 = new IsoValue<String>(IsoType.NUMERIC,
					request.getStan(), 6);
			Log.i("stan", request.getStan());
			IsoValue<String> field12 = new IsoValue<String>(IsoType.TIME,
					request.getLocalTransactionTime());
			Log.i("time", request.getLocalTransactionTime());
			IsoValue<String> field13 = new IsoValue<String>(IsoType.DATE4,
					request.getLocalTransactionDate());
			Log.i("date", request.getLocalTransactionDate());
			IsoValue<String> field14 = new IsoValue<String>(IsoType.DATE4,
					request.getCardExpirationDate());
			Log.i("expdate", request.getCardExpirationDate());
			IsoValue<String> field18 = new IsoValue<String>(IsoType.ALPHA,
					request.getMerchantType(), 4);
			Log.i("mertype", request.getMerchantType());
			IsoValue<String> field22 = new IsoValue<String>(IsoType.ALPHA,
					request.getPosEntryMode(), 3);
			Log.i("posentrymode", request.getPosEntryMode());
			IsoValue<String> field23 = new IsoValue<String>(IsoType.ALPHA,
					request.getCardSequenceNumber(), 3);
			Log.i("seqcardno", request.getCardSequenceNumber());
			IsoValue<String> field25 = new IsoValue<String>(IsoType.ALPHA,
					request.getPosConditionCode(), 2);
			Log.i("poscondtncode", request.getPosConditionCode());
			IsoValue<String> field26 = new IsoValue<String>(IsoType.NUMERIC,
					request.getPosPinCaptureCode(), 2);
			Log.i("pospincapturecode", request.getPosPinCaptureCode());
			IsoValue<String> field28 = new IsoValue<String>(IsoType.ALPHA,
					request.getTransactionFee(), 9);
			Log.i("transactnfee", request.getTransactionFee());
			IsoValue<String> field32 = new IsoValue<String>(IsoType.LLVAR,
					request.getAcquiringInstIdCode());
			Log.i("acqinstcode", request.getAcquiringInstIdCode());
			IsoValue<String> field35 = new IsoValue<String>(IsoType.LLVAR,
					request.getTrack2Data());
			Log.i("track2", request.getTrack2Data());
			IsoValue<String> field37 = new IsoValue<String>(IsoType.ALPHA,
					request.getRetrievalReferenceNumber(), 12);
			Log.i("rrn", request.getRetrievalReferenceNumber());
			IsoValue<String> field40 = new IsoValue<String>(IsoType.NUMERIC,
					request.getServiceRestrictionCode(), 3);
			Log.i("src", request.getServiceRestrictionCode());
			IsoValue<String> field41 = new IsoValue<String>(IsoType.ALPHA,
					request.getTerminalId(), 8);
			Log.i("tid", request.getTerminalId());
			IsoValue<String> field42 = new IsoValue<String>(IsoType.ALPHA,
					request.getCardAcceptorIdCode(), 15);
			Log.i("cac", request.getCardAcceptorIdCode());
			IsoValue<String> field43 = new IsoValue<String>(IsoType.ALPHA,
					request.getCardAcceptorNameLocation(), 40);
			Log.i("canc", request.getCardAcceptorNameLocation());
			IsoValue<String> field49 = new IsoValue<String>(IsoType.NUMERIC,
					request.getCurrencyCode(), 3);
			Log.i("currcode", request.getCurrencyCode());
			IsoValue<String> field52 = new IsoValue<String>(IsoType.ALPHA,
					request.getPinData(), 16);
			IsoValue<String> field55 = new IsoValue<String>(IsoType.LLLVAR,
					request.getIccData());
			Log.i("icc", request.getIccData());
			IsoValue<String> field59 = new IsoValue<String>(IsoType.LLLVAR,
					request.getTransportData());
			Log.i("icc", request.getTransportData());
			IsoValue<String> field123 = new IsoValue<String>(IsoType.LLLVAR,
					request.getPosDataCode());
			Log.i("icc", request.getPosDataCode());
			IsoValue<String> field128 = new IsoValue<String>(IsoType.ALPHA,
					new String(new byte[] { 0x0 }), 64);

			ismsg.setField(2, field2);
			ismsg.setField(3, field3);
			ismsg.setField(4, field4);
			ismsg.setField(7, field7);
			ismsg.setField(11, field11);
			ismsg.setField(12, field12);
			ismsg.setField(13, field13);
			ismsg.setField(14, field14);
			ismsg.setField(18, field18);
			ismsg.setField(22, field22);
			ismsg.setField(23, field23);
			ismsg.setField(25, field25);
			ismsg.setField(26, field26);
			ismsg.setField(28, field28);
			ismsg.setField(32, field32);
			ismsg.setField(35, field35);
			ismsg.setField(37, field37);
			ismsg.setField(40, field40);
			ismsg.setField(41, field41);
			ismsg.setField(42, field42);
			ismsg.setField(43, field43);
			ismsg.setField(49, field49);

			if (request.getPinData() != null) {
				ismsg.setField(52, field52);
			}

			ismsg.setField(55, field55);
			//ismsg.setField(59, field59);
			ismsg.setField(123, field123);
			ismsg.setField(128, field128);

			byte[] bites = ismsg.writeData();

			int length = bites.length;

			/* Hash generation */
			byte[] temp = new byte[length - 64];

			if (length >= 64) {
				System.arraycopy(bites, 0, temp, 0, length - 64);
			}

			String hashHex = NibssIsoProcessor.generateHash256Value(temp,
					sessionKey);

			IsoValue<String> field128update = new IsoValue<String>(
					IsoType.ALPHA, hashHex, 64);

			ismsg.setField(128, field128update);

			byte[] rawMsg = ismsg.writeData();

			int lenght = rawMsg.length;
			byte[] bLenght = new byte[2];
			bLenght[1] = (byte) lenght;
			bLenght[0] = (byte) (lenght >> 8);

			byte[] toSend = concat(bLenght, rawMsg);

			Log.i("PAYPADSDK", new String(toSend));

			TrustManager tm = new X509TrustManager() {
				@Override
	            public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
	            }
				@Override
	            public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
	            }
				@Override
	            public X509Certificate[] getAcceptedIssuers() {
	                return null;
	            }
	        };

	        SSLContext sc = SSLContext.getInstance("SSL");

            sc.init(null, new TrustManager[] { tm }, null);

            SSLSocketFactory ssf = sc.getSocketFactory();
            SSLSocket s = (SSLSocket) ssf.createSocket(Globals.ipAddress, Globals.port);
            s.startHandshake();

            s.setSoTimeout(70*1000);

            Globals.balanceRequest = request;
            // Globals.TSK = sessionKey;

            if (s.isConnected()) {
                System.out.println("I am connected");
                socketPluger = new NibssSocketManager(s);
            }

			socketPluger.sendData(toSend);

			byte[] responseBytes = socketPluger.receive();

			if (responseBytes.equals(null)|| Arrays.equals(responseBytes, compareBytes)) {
				response = new BalanceResponse();
				return response;
			}

			Log.i("PAYPADSDKRESPONSE", new String(responseBytes));

			MessageFactory<IsoMessage> responseMessageFactory = new MessageFactory<IsoMessage>();
			responseMessageFactory.addMessageTemplate(ismsg);
			responseMessageFactory.setAssignDate(true);
			responseMessageFactory.setUseBinaryBitmap(false);
			responseMessageFactory.setUseBinaryMessages(false);
			responseMessageFactory.setEtx(-1);
			responseMessageFactory.setIgnoreLastMissingField(false);

			responseMessageFactory.setConfigPath("assets/isoconfig.xml");

			IsoMessage responseMessage = responseMessageFactory.parseMessage(
					responseBytes, 0);

			if (responseMessage != null) {

				// read response
				response = new BalanceResponse();

				if (responseMessage.hasField(38)) {
					Globals.authCode = (String) responseMessage
							.getObjectValue(38);
				} else {
					Globals.authCode = "Not authenticated";

				}

				if (responseMessage.hasField(54)) {
					String balance = (String) responseMessage
							.getObjectValue(54);
					Log.i("Balance", balance);
				}

				if (responseMessage.hasField(39)) {
					response.setField39((String) responseMessage
							.getObjectValue(39));
				}

				if (responseMessage.hasField(55)) {
					Globals.icc = (String) responseMessage.getObjectValue(55);
					Log.i("PAYPADSDKRESPONSEICC", Globals.icc);

					getICCResponse(Globals.icc);
				}

			}

			Log.i("PAYPADSDK39", response.getField39());

			response.returnMessage();

			Globals.responseCode = response.getField39();
			Globals.appResponse = response.messages.get(response.getField39());

			getPinpadResponse();

		} catch (SocketTimeoutException e) {

			reversal(request, sessionKey);
			response = new BalanceResponse();
			return response;

		} catch (Exception e) {

			e.printStackTrace();
			Globals.responseCode = "XX";
			Globals.appResponse = "UNCOMPLETED TRANSACTION";
			Globals.returnMessage = "UNCOMPLETED\n TRANSACTION";

		}

		return response;
	}

	public static RefundResponse process(RefundRequest request,
			byte[] sessionKey) {

		RefundResponse response = null;
		try {

			IsoMessage ismsg = new IsoMessage();
			ismsg.setType(0x200);
			IsoValue<String> field2 = new IsoValue<String>(IsoType.LLVAR,
					request.getPan());
			Log.i("pan", request.getPan());
			IsoValue<String> field3 = new IsoValue<String>(IsoType.NUMERIC,
					request.getProcessingCode(), 6);
			Log.i("pcode", request.getProcessingCode());
			IsoValue<String> field4 = new IsoValue<String>(IsoType.NUMERIC,
					request.getTransactionAmount(), 12);
			Log.i("amt", request.getTransactionAmount());
			IsoValue<String> field7 = new IsoValue<String>(IsoType.DATE10,
					request.getTransmissionDateTime());
			Log.i("dandt", request.getTransmissionDateTime());
			IsoValue<String> field11 = new IsoValue<String>(IsoType.NUMERIC,
					request.getStan(), 6);
			Log.i("stan", request.getStan());
			IsoValue<String> field12 = new IsoValue<String>(IsoType.TIME,
					request.getLocalTransactionTime());
			Log.i("time", request.getLocalTransactionTime());
			IsoValue<String> field13 = new IsoValue<String>(IsoType.DATE4,
					request.getLocalTransactionDate());
			Log.i("date", request.getLocalTransactionDate());
			IsoValue<String> field14 = new IsoValue<String>(IsoType.DATE4,
					request.getCardExpirationDate());
			Log.i("expdate", request.getCardExpirationDate());
			IsoValue<String> field18 = new IsoValue<String>(IsoType.ALPHA,
					request.getMerchantType(), 4);
			Log.i("mertype", request.getMerchantType());
			IsoValue<String> field22 = new IsoValue<String>(IsoType.ALPHA,
					request.getPosEntryMode(), 3);
			Log.i("posentrymode", request.getPosEntryMode());
			IsoValue<String> field23 = new IsoValue<String>(IsoType.ALPHA,
					request.getCardSequenceNumber(), 3);
			Log.i("seqcardno", request.getCardSequenceNumber());
			IsoValue<String> field25 = new IsoValue<String>(IsoType.ALPHA,
					request.getPosConditionCode(), 2);
			Log.i("poscondtncode", request.getPosConditionCode());
			IsoValue<String> field26 = new IsoValue<String>(IsoType.NUMERIC,
					request.getPosPinCaptureCode(), 2);
			Log.i("pospincapturecode", request.getPosPinCaptureCode());
			IsoValue<String> field28 = new IsoValue<String>(IsoType.ALPHA,
					request.getTransactionFee(), 9);
			Log.i("transactnfee", request.getTransactionFee());
			IsoValue<String> field32 = new IsoValue<String>(IsoType.LLVAR,
					request.getAcquiringInstIdCode());
			Log.i("acqinstcode", request.getAcquiringInstIdCode());
			IsoValue<String> field35 = new IsoValue<String>(IsoType.LLVAR,
					request.getTrack2Data());
			Log.i("track2", request.getTrack2Data());
			IsoValue<String> field37 = new IsoValue<String>(IsoType.ALPHA,
					request.getRetrievalReferenceNumber(), 12);
			Log.i("rrn", request.getRetrievalReferenceNumber());
			IsoValue<String> field40 = new IsoValue<String>(IsoType.NUMERIC,
					request.getServiceRestrictionCode(), 3);
			Log.i("src", request.getServiceRestrictionCode());
			IsoValue<String> field41 = new IsoValue<String>(IsoType.ALPHA,
					request.getTerminalId(), 8);
			Log.i("tid", request.getTerminalId());
			IsoValue<String> field42 = new IsoValue<String>(IsoType.ALPHA,
					request.getCardAcceptorIdCode(), 15);
			Log.i("cac", request.getCardAcceptorIdCode());
			IsoValue<String> field43 = new IsoValue<String>(IsoType.ALPHA,
					request.getCardAcceptorNameLocation(), 40);
			Log.i("canc", request.getCardAcceptorNameLocation());
			IsoValue<String> field49 = new IsoValue<String>(IsoType.NUMERIC,
					request.getCurrencyCode(), 3);
			Log.i("currcode", request.getCurrencyCode());
			IsoValue<String> field52 = new IsoValue<String>(IsoType.ALPHA,
					request.getPinData(), 16);
			IsoValue<String> field55 = new IsoValue<String>(IsoType.LLLVAR,
					request.getIccData());
			Log.i("icc", request.getIccData());
			IsoValue<String> field59 = new IsoValue<String>(IsoType.LLLVAR,
					request.getTransportData());
			Log.i("icc", request.getTransportData());
			IsoValue<String> field95 = new IsoValue<String>(IsoType.NUMERIC,
					request.getReplacementAmount(), 42);
			Log.i("Replacement Amt", request.getTransportData());
			IsoValue<String> field123 = new IsoValue<String>(IsoType.LLLVAR,
					request.getPosDataCode());
			Log.i("icc", request.getPosDataCode());
			IsoValue<String> field128 = new IsoValue<String>(IsoType.ALPHA,
					new String(new byte[] { 0x0 }), 64);

			ismsg.setField(2, field2);
			ismsg.setField(3, field3);
			ismsg.setField(4, field4);
			ismsg.setField(7, field7);
			ismsg.setField(11, field11);
			ismsg.setField(12, field12);
			ismsg.setField(13, field13);
			ismsg.setField(14, field14);
			ismsg.setField(18, field18);
			ismsg.setField(22, field22);
			ismsg.setField(23, field23);
			ismsg.setField(25, field25);
			ismsg.setField(26, field26);
			ismsg.setField(28, field28);
			ismsg.setField(32, field32);
			ismsg.setField(35, field35);
			ismsg.setField(37, field37);
			ismsg.setField(40, field40);
			ismsg.setField(41, field41);
			ismsg.setField(42, field42);
			ismsg.setField(43, field43);
			ismsg.setField(49, field49);

			if (request.getPinData() != null) {
				ismsg.setField(52, field52);
			}

			ismsg.setField(55, field55);
			ismsg.setField(95, field95);
			ismsg.setField(123, field123);
			ismsg.setField(128, field128);

			byte[] bites = ismsg.writeData();

			int length = bites.length;

			/* Hash generation */
			byte[] temp = new byte[length - 64];

			if (length >= 64) {
				System.arraycopy(bites, 0, temp, 0, length - 64);
			}

			String hashHex = NibssIsoProcessor.generateHash256Value(temp,
					sessionKey);

			IsoValue<String> field128update = new IsoValue<String>(
					IsoType.ALPHA, hashHex, 64);

			ismsg.setField(128, field128update);

			byte[] rawMsg = ismsg.writeData();

			int lenght = rawMsg.length;
			byte[] bLenght = new byte[2];
			bLenght[1] = (byte) lenght;
			bLenght[0] = (byte) (lenght >> 8);

			byte[] toSend = concat(bLenght, rawMsg);

			Log.i("PAYPADSDK", new String(toSend));

			TrustManager tm = new X509TrustManager() {
				@Override
	            public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
	            }
				@Override
	            public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
	            }
				@Override
	            public X509Certificate[] getAcceptedIssuers() {
	                return null;
	            }
	        };

	        SSLContext sc = SSLContext.getInstance("SSL");

            sc.init(null, new TrustManager[] { tm }, null);

            SSLSocketFactory ssf = sc.getSocketFactory();
            SSLSocket s = (SSLSocket) ssf.createSocket(Globals.ipAddress, Globals.port);
            s.startHandshake();

            s.setSoTimeout(75*1000);

            Globals.refundRequest = request;
            // Globals.TSK = sessionKey;

            if (s.isConnected()) {
                System.out.println("I am connected");
                socketPluger = new NibssSocketManager(s);

            }

			socketPluger.sendData(toSend);

			byte[] responseBytes = socketPluger.receive();

			if (responseBytes.equals(null)|| Arrays.equals(responseBytes, compareBytes)) {
				response = new RefundResponse();
				return response;
			}

			Log.i("PAYPADSDKRESPONSE", new String(responseBytes));

			MessageFactory<IsoMessage> responseMessageFactory = new MessageFactory<IsoMessage>();
			responseMessageFactory.addMessageTemplate(ismsg);
			responseMessageFactory.setAssignDate(true);
			responseMessageFactory.setUseBinaryBitmap(false);
			responseMessageFactory.setUseBinaryMessages(false);
			responseMessageFactory.setEtx(-1);
			responseMessageFactory.setIgnoreLastMissingField(false);

			responseMessageFactory.setConfigPath("assets/isoconfig.xml");

			IsoMessage responseMessage = responseMessageFactory.parseMessage(
					responseBytes, 0);

			if (responseMessage != null) {

				// read response
				response = new RefundResponse();

				if (responseMessage.hasField(38)) {
					Globals.authCode = (String) responseMessage
							.getObjectValue(38);
				} else {
					Globals.authCode = "Not authenticated";

				}

				if (responseMessage.hasField(54)) {
					String balance = (String) responseMessage
							.getObjectValue(54);
					Log.i("Balance", balance);
				}

				if (responseMessage.hasField(39)) {
					response.setField39((String) responseMessage
							.getObjectValue(39));
				}

				if (responseMessage.hasField(55)) {
					Globals.icc = (String) responseMessage.getObjectValue(55);
					Log.i("PAYPADSDKRESPONSEICC", Globals.icc);

					getICCResponse(Globals.icc);
				}

			}

			Log.i("PAYPADSDK39", response.getField39());

			response.returnMessage();

			Globals.responseCode = response.getField39();
			Globals.appResponse = response.messages.get(response.getField39());

			getPinpadResponse();

		} catch (SocketTimeoutException e) {

			reversal(request, sessionKey);
			response = new RefundResponse();
			return response;

		} catch (Exception e) {

			e.printStackTrace();
			Globals.responseCode = "XX";
			Globals.appResponse = "UNCOMPLETED TRANSACTION";
			Globals.returnMessage = "UNCOMPLETED\n TRANSACTION";

		}

		return response;
	}

	public static void reversal(_0200Request request, byte[] sessionKey) {
		ReversalResponse response = null;
		try {

			String transmissionDateTimexx = String.valueOf(System.currentTimeMillis());

			String retrievalReferenceNumber = transmissionDateTimexx;

			int len = retrievalReferenceNumber.length();
			int pad = 0;
			if (len < 12) {

				pad = 12 - len;

				for (int i = 0; i < pad; i++) {
					retrievalReferenceNumber = "0" + retrievalReferenceNumber;
				}
			}

			if(len > 12){
				retrievalReferenceNumber = retrievalReferenceNumber.substring(len -12);
			}


			Globals.rrn = Long.parseLong(retrievalReferenceNumber);
			String tester = retrievalReferenceNumber.substring(retrievalReferenceNumber.length()-6);
			Globals.stan = Long.parseLong(tester);

			Calendar date = Calendar.getInstance();
			Date transdate = date.getTime();
			SimpleDateFormat format = new SimpleDateFormat("hhmmss");
			String localTransactionTime = format.format(transdate);
			int day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
			int month = Calendar.getInstance().get(Calendar.MONTH) + 1;
			String stringDay = String.valueOf(day);
			String stringMonth = String.valueOf(month);
			if (stringDay.length() == 1) {
				stringDay = "0" + stringDay;
			}

			if (stringMonth.length() == 1) {
				stringMonth = "0" + stringMonth;
			}
			String localTransactionDate = stringMonth + stringDay;
			String transmissionDateTime = localTransactionDate
					+ localTransactionTime;

			IsoMessage ismsg = new IsoMessage();
			ismsg.setType(0x420);
			IsoValue<String> field2 = new IsoValue<String>(IsoType.LLVAR,
					request.getPan());
			IsoValue<String> field3 = new IsoValue<String>(IsoType.NUMERIC,
					"000000", 6);
			IsoValue<String> field4 = new IsoValue<String>(IsoType.NUMERIC,
					request.getTransactionAmount(), 12);
			IsoValue<String> field7 = new IsoValue<String>(IsoType.DATE10,
					transmissionDateTime);
			IsoValue<String> field11 = new IsoValue<String>(IsoType.NUMERIC,
					String.valueOf(Globals.stan), 6);
			IsoValue<String> field12 = new IsoValue<String>(IsoType.TIME,
					localTransactionTime);
			IsoValue<String> field13 = new IsoValue<String>(IsoType.DATE4,
					localTransactionDate);
			IsoValue<String> field14 = new IsoValue<String>(IsoType.DATE4,
					request.getCardExpirationDate());
			IsoValue<String> field18 = new IsoValue<String>(IsoType.ALPHA,
					request.getMerchantType(), 4);
			IsoValue<String> field22 = new IsoValue<String>(IsoType.ALPHA,
					request.getPosEntryMode(), 3);
			IsoValue<String> field23 = new IsoValue<String>(IsoType.ALPHA,
					request.getCardSequenceNumber(), 3);
			IsoValue<String> field25 = new IsoValue<String>(IsoType.ALPHA,
					request.getPosConditionCode(), 2);
			IsoValue<String> field26 = new IsoValue<String>(IsoType.NUMERIC,
					request.getPosPinCaptureCode(), 2);
			IsoValue<String> field32 = new IsoValue<String>(IsoType.LLVAR,
					request.getAcquiringInstIdCode());
			IsoValue<String> field37 = new IsoValue<String>(IsoType.ALPHA,
					String.valueOf(Globals.rrn), 12);
			IsoValue<String> field40 = new IsoValue<String>(IsoType.NUMERIC,
					request.getServiceRestrictionCode(), 3);
			IsoValue<String> field41 = new IsoValue<String>(IsoType.ALPHA,
					request.getTerminalId(), 8);
			IsoValue<String> field42 = new IsoValue<String>(IsoType.ALPHA,
					request.getCardAcceptorIdCode(), 15);
			IsoValue<String> field43 = new IsoValue<String>(IsoType.ALPHA,
					request.getCardAcceptorNameLocation(), 40);
			IsoValue<String> field49 = new IsoValue<String>(IsoType.NUMERIC,
					request.getCurrencyCode(), 3);
			IsoValue<String> field90 = new IsoValue<String>(IsoType.NUMERIC,
					request.getOriginalDataElements(), 42);
			IsoValue<String> field123 = new IsoValue<String>(IsoType.LLLVAR,
					request.getPosDataCode());
			IsoValue<String> field128 = new IsoValue<String>(IsoType.ALPHA,
					new String(new byte[] { 0x0 }), 64);

			ismsg.setField(2, field2);
			ismsg.setField(3, field3);
			ismsg.setField(4, field4);
			ismsg.setField(7, field7);
			ismsg.setField(11, field11);
			ismsg.setField(12, field12);
			ismsg.setField(13, field13);
			ismsg.setField(14, field14);
			ismsg.setField(18, field18);
			ismsg.setField(22, field22);
			ismsg.setField(23, field23);
			ismsg.setField(25, field25);
			ismsg.setField(26, field26);
			ismsg.setField(32, field32);
			ismsg.setField(37, field37);
			Log.i("PAYPADSDK", field37.toString());
			ismsg.setField(40, field40);
			ismsg.setField(41, field41);
			ismsg.setField(42, field42);
			ismsg.setField(43, field43);
			ismsg.setField(49, field49);
			ismsg.setField(90, field90);
			ismsg.setField(123, field123);
			ismsg.setField(128, field128);

			byte[] bites = ismsg.writeData();

			int length = bites.length;

			/* Hash generation */
			byte[] temp = new byte[length - 64];

			if (length >= 64) {
				System.arraycopy(bites, 0, temp, 0, length - 64);
			}

			String hashHex = NibssIsoProcessor.generateHash256Value(temp,
					sessionKey);

			IsoValue<String> field128update = new IsoValue<String>(
					IsoType.ALPHA, hashHex, 64);

			// Update field 128
			ismsg.setField(128, field128update);

			byte[] rawMsg = ismsg.writeData();

			int lenght = rawMsg.length;
			byte[] bLenght = new byte[2];
			bLenght[1] = (byte) lenght; // (lenght & 0xFF);
			bLenght[0] = (byte) (lenght >> 8); // ((lenght >> 8) & 0xFF);
			byte[] toSend = concat(bLenght, rawMsg);

			String dd = new String(toSend);

			Log.i("PAYPADSDK", new String(toSend));

			TrustManager tm = new X509TrustManager() {
				@Override
	            public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
	            }
				@Override
	            public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
	            }
				@Override
	            public X509Certificate[] getAcceptedIssuers() {
	                return null;
	            }
	        };

	        // SSLContext sc = SSLContext.getInstance("TLS");

         //    sc.init(null, new TrustManager[] { tm }, null);

         //    SSLSocketFactory ssf = sc.getSocketFactory();

         //    Socket s = new Socket(Globals.ipAddress, Globals.port);

         //    s.startHandshake();

	        SSLContext sc = SSLContext.getInstance("TLS");

            sc.init(null, new TrustManager[] { tm }, null);

            SSLSocketFactory ssf = sc.getSocketFactory();

            // PaypadFacade.showStaticToastLong("reversal - "+Globals.ipAddress+" : "+Globals.port);

            SSLSocket s = (SSLSocket) ssf.createSocket(Globals.ipAddress, Globals.port);

            // Socket s = new Socket(Globals.ipAddress, Globals.port);
            s.startHandshake();

            // s.setSoTimeout(70*1000);

            // Globals.purchaseRequest = request;

            s.setSoTimeout(70*1000);

            if (s.isConnected()) {

            	// PaypadFacade.showStaticToastLong("connected to server reversal");
                System.out.println("I am connected");
                socketPluger = new NibssSocketManager(s);

            }

			socketPluger.sendData(toSend);

			byte[] responseBytes = socketPluger.receive();

			Log.i("PAYPADSDKRESPONSE", new String(responseBytes));

			MessageFactory<IsoMessage> responseMessageFactory = new MessageFactory<IsoMessage>();
			responseMessageFactory.addMessageTemplate(ismsg);
			responseMessageFactory.setAssignDate(true);
			responseMessageFactory.setUseBinaryBitmap(false);
			responseMessageFactory.setUseBinaryMessages(false);
			responseMessageFactory.setEtx(-1);
			responseMessageFactory.setIgnoreLastMissingField(false);

			responseMessageFactory.setConfigPath("assets/www/isoconfig.xml");

			IsoMessage responseMessage = responseMessageFactory.parseMessage(
					responseBytes, 0);

			response = new ReversalResponse();

			if (responseMessage != null) {

				// read response
				if (responseMessage.hasField(39)) {
					response.setField39((String) responseMessage
							.getObjectValue(39));
				}

			}

			Log.i("PAYPADSDK39", response.getField39());


			String code = response.getField39();

		} catch (Exception e) {

			if(e != null){
				// PaypadFacade.showStaticToastLong(""+e.getMessage());
				Log.i("ISOPACKEGE", e.getMessage());
				e.printStackTrace();
			}

		}

		increment();

	}

	public static String generateHash256Value(byte[] iso, byte[] key) {

		MessageDigest m = null;
		String hashText = null;

		try {
			m = MessageDigest.getInstance("SHA-256");
			m.update(key, 0, key.length);
			m.update(iso, 0, iso.length);
			// hashText = new BigInteger(1, m.digest()).toString(16);
			hashText = HexUtil.byteArrayToHexString(m.digest());
			hashText = hashText.replace(" ", "");

		} catch (NoSuchAlgorithmException ex) {
			ex.printStackTrace();
		}

		if (hashText.length() < 64) {
			int numberOfZeroes = 64 - hashText.length();
			String zeroes = "";
			String temp = hashText.toString();

			for (int i = 0; i < numberOfZeroes; i++)
				zeroes = zeroes + "0";

			temp = zeroes + temp;

			Log.i("Utility :: generateHash256Value :: HashValue with zeroes: ",
					temp);
			return temp;
		}

		return hashText;
	}

	private static byte[] concat(byte[] A, byte[] B) {
		int aLen = A.length;
		int bLen = B.length;
		byte[] C = new byte[aLen + bLen];
		System.arraycopy(A, 0, C, 0, aLen);
		System.arraycopy(B, 0, C, aLen, bLen);
		return C;
	}

	@TargetApi(Build.VERSION_CODES.GINGERBREAD)
    public static void getICCResponse(String icc) {

		int index91 = 4 + 20;
		int index71, index72 = 0;

		if (icc.substring(0, 2).equals("91")) {

			if (icc.substring(0, 4).equals("9110")) {

				// 91 has length 10 but actual value is less than 20
				if (icc.substring(4).length() < 20) {

					Globals.authData = icc.substring(4);

					// 91 has length 10 and value = 20
				} else if (icc.substring(4).length() == 20) {

					Globals.authData = icc.substring(4, index91);

					// ICC also has issuer script
				} else {

					// get value of 91
					Globals.authData = icc.substring(4, index91);

					// check if script has 71
					if (icc.substring(index91, index91 + 2).equals("71")) {

						String hexLent = icc
								.substring(index91 + 2, index91 + 4);
						int lent = Integer.parseInt(hexLent, 16) * 2;

						index71 = lent + 4;// lenth of V + length of T and L

						Globals.script71 = icc.substring(index91, index71);

						// check if 72 follows 71
						if (!icc.substring(index71).isEmpty()) {

							Globals.script72 = icc.substring(index71);

						}

					}

					// if 91 is followed by only 72
					else if (icc.substring(index91, index91 + 2).equals("72")) {

						Globals.script72 = icc.substring(index91);

					}

				}

			} else {// length of 91 is sent in hexadecimal

				String hexLent = icc.substring(2, 4);
				int lent = Integer.parseInt(hexLent, 16) * 2;

				index91 = lent + 4;

				Globals.authData = icc.substring(4, index91);

				if (!icc.substring(index91).isEmpty()) {

					// check if script has 71
					if (icc.substring(index91, index91 + 2).equals("71")) {
						String hexLent1 = icc.substring(index91 + 2,
								index91 + 4);
						int lent1 = Integer.parseInt(hexLent1, 16) * 2;

						index71 = lent1 + 4;// lenth of V + length of T and L

						Globals.script71 = icc.substring(index91, index71);

						// check if 72 follows 71
						if (!icc.substring(index71).isEmpty()) {

							Globals.script72 = icc.substring(index71);

						}

					}

					// if 91 is followed by only 72
					if (icc.substring(index91, index91 + 2).equals("72")) {

						Globals.script72 = icc.substring(index91);

					}
				}

			}
		} else {// Icc has no 91

			if (icc.substring(0, 2).equals("71")) {

				String hexLent1 = icc.substring(2, 4);
				int lent1 = Integer.parseInt(hexLent1, 16) * 2;

				index71 = lent1 + 4;// lenth of V + length of T and L

				Globals.script71 = icc.substring(0, index71);

				// check if 72 follows 71
				if (!icc.substring(index71).isEmpty()) {

					Globals.script72 = icc.substring(index71);

				}

			}

			// if script consist of only 72
			if (icc.substring(0, 2).equals("72")) {

				Globals.script72 = icc;

			}
		}
	}

	public static void getPinpadResponse() {

		if (Globals.responseCode.equals("00")) {
			Globals.returnMessage = "TRANSACTION\n   SUCCESSFUL";
		} else if (Globals.responseCode.equals("01")) {
			Globals.returnMessage = "   REFER TO\n  CARD ISSUER";
		} else if (Globals.responseCode.equals("02")) {
			Globals.returnMessage = "   REFER TO\n  CARD ISSUER";
		} else if (Globals.responseCode.equals("03")) {
			Globals.returnMessage = "   INVALID\n    MERCHANT   ";
		} else if (Globals.responseCode.equals("04")) {
			Globals.returnMessage = "  PICK-UP\n      CARD   ";
		} else if (Globals.responseCode.equals("05")) {
			Globals.returnMessage = "    DO NOT\n   HONOUR";
		} else if (Globals.responseCode.equals("06")) {
			Globals.returnMessage = "      ERROR   ";
		} else if (Globals.responseCode.equals("07")) {
			Globals.returnMessage = "   PICK-UP\n      CARD   ";
		} else if (Globals.responseCode.equals("08")) {
			Globals.returnMessage = "     HONOUR\n    WITH ID";
		} else if (Globals.responseCode.equals("09")) {
			Globals.returnMessage = "    REQUEST IN\n    PROGRESS   ";
		} else if (Globals.responseCode.equals("10")) {
			Globals.returnMessage = "   APPROVED,\n   PARTIAL";
		} else if (Globals.responseCode.equals("11")) {
			Globals.returnMessage = "   APPROVED,\n     VIP";
		} else if (Globals.responseCode.equals("12")) {
			Globals.returnMessage = "   INVALID\n   TRANSACTION";
		} else if (Globals.responseCode.equals("13")) {
			Globals.returnMessage = "   INVALID\n     AMOUNT   ";
		} else if (Globals.responseCode.equals("14")) {
			Globals.returnMessage = " INVALID CARD\n    NUMBER   ";
		} else if (Globals.responseCode.equals("15")) {
			Globals.returnMessage = "  NO SUCH\n    ISSUER   ";
		} else if (Globals.responseCode.equals("16")) {
			Globals.returnMessage = "    APPROVED\n UPDATE TRACK3";
		} else if (Globals.responseCode.equals("17")) {
			Globals.returnMessage = "   CUSTOMER\n  CANCELLATION   ";
		} else if (Globals.responseCode.equals("18")) {
			Globals.returnMessage = "   CUSTOMER\n    DISPUTE   ";
		} else if (Globals.responseCode.equals("19")) {
			Globals.returnMessage = "      RE-ENTER\n   TRANSACTION";
		} else if (Globals.responseCode.equals("20")) {
			Globals.returnMessage = "     INVALID\n     RESPONSE   ";
		} else if (Globals.responseCode.equals("21")) {
			Globals.returnMessage = "  NO ACTION\n     TAKEN";
		} else if (Globals.responseCode.equals("22")) {
			Globals.returnMessage = "  SUSPECTED\n   MALFUNCTION";
		} else if (Globals.responseCode.equals("23")) {
			Globals.returnMessage = "UNACCEPTABLE FEE\n FOR TRANSACTION";
		} else if (Globals.responseCode.equals("24")) {
			Globals.returnMessage = " FILE UPDATE\n  NOT SUPPORTED    ";
		} else if (Globals.responseCode.equals("25")) {
			Globals.returnMessage = "  UNABLE TO\n LOCATE RECORD   ";
		} else if (Globals.responseCode.equals("26")) {
			Globals.returnMessage = "   DUPLICATE\n      RECORD   ";
		} else if (Globals.responseCode.equals("27")) {
			Globals.returnMessage = " FILE UPDATE\n  EDIT ERROR  ";
		} else if (Globals.responseCode.equals("28")) {
			Globals.returnMessage = " FILE UPDATE\n  FILE LOCKED  ";
		} else if (Globals.responseCode.equals("29")) {
			Globals.returnMessage = " FILE UPDATE\n     FAILED";
		} else if (Globals.responseCode.equals("30")) {
			Globals.returnMessage = "      FORMAT\n      ERROR";
		} else if (Globals.responseCode.equals("31")) {
			Globals.returnMessage = "    BANK NOT\n    SUPPORTED   ";
		} else if (Globals.responseCode.equals("32")) {
			Globals.returnMessage = "   COMPLETED\n    PARTIALLY";
		} else if (Globals.responseCode.equals("33")) {
			Globals.returnMessage = "  EXPIRED CARD\n    PICK-UP";
		} else if (Globals.responseCode.equals("34")) {
			Globals.returnMessage = " SUSPECTED FRAUD\n  PICK-UP   ";
		} else if (Globals.responseCode.equals("35")) {
			Globals.returnMessage = " CONTACT ACQUIRER\n  PICK-UP   ";
		} else if (Globals.responseCode.equals("36")) {
			Globals.returnMessage = " RESTRICTED CARD\n  PICK-UP   ";
		} else if (Globals.responseCode.equals("37")) {
			Globals.returnMessage = "CALL ACQUIRER SECURITY PICK-UP   ";
		} else if (Globals.responseCode.equals("38")) {
			Globals.returnMessage = "PIN TRIES EXCEEDED\n  PICK-UP   ";
		} else if (Globals.responseCode.equals("39")) {
			Globals.returnMessage = "   NO CREDIT\n    ACCOUNT   ";
		} else if (Globals.responseCode.equals("40")) {
			Globals.returnMessage = "  FUNCTION NOT\n   SUPPORTED   ";
		} else if (Globals.responseCode.equals("41")) {
			Globals.returnMessage = "   LOST CARD";
		} else if (Globals.responseCode.equals("42")) {
			Globals.returnMessage = "  NO UNIVERSAL\n     ACCOUNT   ";
		} else if (Globals.responseCode.equals("43")) {
			Globals.returnMessage = " STOLEN CARD";
		} else if (Globals.responseCode.equals("44")) {
			Globals.returnMessage = " NO INVESTMENT\n      ACCOUNT";
		} else if (Globals.responseCode.equals("51")) {
			Globals.returnMessage = "NOT SUFFICIENT\n FUNDS";
		} else if (Globals.responseCode.equals("52")) {
			Globals.returnMessage = "  NO CURRENT\n    ACCOUNT   ";
		} else if (Globals.responseCode.equals("53")) {
			Globals.returnMessage = "  NO SAVINGS\n    ACCOUNT   ";
		} else if (Globals.responseCode.equals("54")) {
			Globals.returnMessage = " EXPIRED CARD ";
		} else if (Globals.responseCode.equals("55")) {
			Globals.returnMessage = "   INCORRECT\n        PIN   ";
		} else if (Globals.responseCode.equals("56")) {
			Globals.returnMessage = "  NO CARD\n      RECORD   ";
		} else if (Globals.responseCode.equals("57")
				|| Globals.responseCode.equals("58")) {
			Globals.returnMessage = "TRANSACTION\n  NOT PERMITTED";
		} else if (Globals.responseCode.equals("59")) {
			Globals.returnMessage = "    SUSPECTED\n      FRAUD ";
		} else if (Globals.responseCode.equals("60")) {
			Globals.returnMessage = "    CONTACT\n       ACQUIRER   ";
		} else if (Globals.responseCode.equals("61")) {
			Globals.returnMessage = "EXCEEDS WITHDRAWAL\n       LIMIT   ";
		} else if (Globals.responseCode.equals("62")) {
			Globals.returnMessage = "   RESTRICTED\n        CARD   ";
		} else if (Globals.responseCode.equals("63")) {
			Globals.returnMessage = "   SECURITY\n   VIOLATION   ";
		} else if (Globals.responseCode.equals("64")) {
			Globals.returnMessage = " ORIGINAL AMOUNT\n     INCORRECT   ";
		} else if (Globals.responseCode.equals("65")) {
			Globals.returnMessage = "EXCEEDS WITHDRAWAL\n   FREQUENCY   ";
		} else if (Globals.responseCode.equals("66")) {
			Globals.returnMessage = "  CALL ACQUIRER\n      SECURITY  ";
		} else if (Globals.responseCode.equals("67")) {
			Globals.returnMessage = "HARD CAPTURE";
		} else if (Globals.responseCode.equals("68")) {
			Globals.returnMessage = "RESPONSE RECEIVED\n   TOO LATE ";
		} else if (Globals.responseCode.equals("75")) {
			Globals.returnMessage = "   PIN TRY\n    EXCEEDED  ";
		} else if (Globals.responseCode.equals("77")) {
			Globals.returnMessage = "BANK INTERVENE";
		} else if (Globals.responseCode.equals("78")) {
			Globals.returnMessage = "BANK INTERVENE";
		} else if (Globals.responseCode.equals("90")) {
			Globals.returnMessage = "   CUT-OFF\n  IN PROGRESS  ";
		} else if (Globals.responseCode.equals("91")) {
			Globals.returnMessage = "   SWITCH\n  INOPERATIVE   ";
		} else if (Globals.responseCode.equals("92")) {
			Globals.returnMessage = "     ROUTING\n      ERROR";
		} else if (Globals.responseCode.equals("93")) {
			Globals.returnMessage = "LAW VIOLATION";
		} else if (Globals.responseCode.equals("94")) {
			Globals.returnMessage = "    DUPLICATE\n   TRANSACTION   ";
		} else if (Globals.responseCode.equals("95")) {
			Globals.returnMessage = "    RECONCILE\n      ERROR";
		} else if (Globals.responseCode.equals("96")) {
			Globals.returnMessage = "   SYSTEM\n   MALFUNCTION";
		} else if (Globals.responseCode.equals("98")) {
			Globals.returnMessage = "  EXCEEDS CASH\n      LIMIT";
		}

	}

	private static void increment() {


		String stan = Globals.stan + "";
		String rrn = Globals.rrn + "";

		long counter = Long.parseLong(stan);
		long rrnCounter = Long.parseLong(rrn);

		counter++;
		rrnCounter = rrnCounter + 1;

		if (counter > 999999) {
			counter = 000000;
		}
		if (rrnCounter > 999999999999L) {
			counter = 000000000000;
		}

		Globals.stan = counter;
		Globals.rrn = rrnCounter;

		SharedPreferences.Editor editor = ReferenceList.config.edit();
        editor.putString(Globals.stanKey, Globals.stan+"");
        editor.putString(Globals.rrnKey, Globals.rrn+"");
        editor.commit();

//		MiscUtils.storeInSharedPreferences(preferencesName, ReferenceList.stan,
//				String.valueOf(Globals.stan));
//		MiscUtils.storeInSharedPreferences(preferencesName, ReferenceList.rrn,
//				String.valueOf(Globals.rrn));

	}

	private static void logToWebHook(Exception ex){
        // Send to web-hook       
		 try {            
			JSONObject paramsJsonObj = new JSONObject();            
			 
		    paramsJsonObj.put("amount", Globals.amount);
            paramsJsonObj.put("agent", Globals.merchantTerminalID);
            paramsJsonObj.put("date", Globals.transDate);
            paramsJsonObj.put("error", ex.getMessage());
            paramsJsonObj.put("stackTrace", Arrays.toString(ex.getStackTrace()));
            paramsJsonObj.put("responseCode", Globals.responseCode);            
			Log.i("WebHook", paramsJsonObj.toString());           
			   
			URL url = new URL("http://138.197.111.151:8383/ErrorLogger/request"); 
			HttpURLConnection conn = (HttpURLConnection) 
			url.openConnection();
			conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setUseCaches(false);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");            
			byte[] postData = paramsJsonObj.toString().getBytes();
            OutputStream os = conn.getOutputStream();            
			os.write(postData);            
			os.flush();
            os.close();
            long result = (long) conn.getResponseCode();
            Log.i("RESULT", Long.toString(result));
//                boolean check = paramsJsonObj.optBoolean("status");
//                System.out.print(result);
        
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }   
		
}

	
}



