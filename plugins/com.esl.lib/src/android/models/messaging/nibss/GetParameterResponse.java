package models.messaging.nibss;

public class GetParameterResponse {
	
	public String field39;
	public String field62;
	
	public void setField39(String field39){
		this.field39 = field39; 
	}
	
	public String getField39(){
		return field39;
	}
	
	public void setField62(String field62){
		this.field62 = field62; 
	}
	
	public String getField62(){
		return field62;
	}
	
	
}
