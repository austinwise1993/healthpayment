package models.messaging.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(value = RetentionPolicy.RUNTIME)
public @interface ISO8583Field {
	
	int value()  default 0;
    boolean isMandatory() default true;

}
