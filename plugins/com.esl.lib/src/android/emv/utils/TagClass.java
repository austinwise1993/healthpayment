package emv.utils;

/**
 * Tag class type
 */
public enum TagClass {
    UNIVERSAL, 
    APPLICATION, 
    CONTEXT_SPECIFIC, 
    PRIVATE
}