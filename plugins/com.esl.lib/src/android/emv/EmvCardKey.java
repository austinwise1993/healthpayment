package emv;


public class EmvCardKey {

	private int keyId;
	private int hashInd;
	private int arithInd;	
	private String modulus;	
	private String exponent;
	private String expDate;
	private String checkSum;
	
	
	public int getKeyId() {
		return keyId;
	}
	public void setKeyId(int keyId) {
		this.keyId = keyId;
	}
	public int getHashInd() {
		return hashInd;
	}
	public void setHashInd(int hashInd) {
		this.hashInd = hashInd;
	}
	public int getArithInd() {
		return arithInd;
	}
	public void setArithInd(int arithInd) {
		this.arithInd = arithInd;
	}
	public String getModulus() {
		return modulus;
	}
	public void setModulus(String modulus) {
		this.modulus = modulus;
	}
	public String getExponent() {
		return exponent;
	}
	public void setExponent(String exponent) {
		this.exponent = exponent;
	}
	public String getExpDate() {
		return expDate;
	}
	public void setExpDate(String expDate) {
		this.expDate = expDate;
	}
	public String getCheckSum() {
		return checkSum;
	}
	public void setCheckSum(String checkSum) {
		this.checkSum = checkSum;
	}
	
	
	
}
