package emv;

import java.util.ArrayList;
import java.util.List;

public class EmvCardApplication {
	
	private String aid;
	private String appName;
	private boolean partialMatch;
	private String version;	
	private List<EmvCardKey> cardKeys = new ArrayList<EmvCardKey>();
	
	public String getAid() {
		return aid;
	}
	public void setAid(String aid) {
		this.aid = aid;
	}
	public String getAppName() {
		return appName;
	}
	public void setAppName(String appName) {
		this.appName = appName;
	}
	public boolean isPartialMatch() {
		return partialMatch;
	}
	public void setPartialMatch(boolean partialMatch) {
		this.partialMatch = partialMatch;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public List<EmvCardKey> getCardKeys() {
		return cardKeys;
	}
	public void setCardKeys(List<EmvCardKey> cardKeys) {
		this.cardKeys = cardKeys;
	}

	
	
}
