//
//  NibssIsoProcessor.h
//  MyBluetoothTest
//
//  Created by Emmanuel Adeyemi on 2/9/16.
//  Copyright © 2016 Emmanuel Adeyemi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonDigest.h>
#import "PurchaseRequest.h"
#import "ISODataElement.h"
#import "ISOBitmap.h"
#import "ISOMessage.h"
#import "PurchaseResponse.h"

@interface NibssIsoProcessor : NSObject

+(PurchaseResponse*) process :(PurchaseRequest *) request;
+(NSString *)generateHash256Value:(NSData *) iso sessionKey:(NSData *)sessionKey;

@end
