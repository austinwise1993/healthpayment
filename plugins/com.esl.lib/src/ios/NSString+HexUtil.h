//
//  NSString+HexUtil.h
//  MyBluetoothTest
//
//  Created by Emmanuel Adeyemi on 1/8/16.
//  Copyright © 2016 Emmanuel Adeyemi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (HexUtil)

-(NSData*) hexStringToData;
-(NSData*) stringToData;
-(NSData*) hexStringToData:(NSString*) delimiter;
+(NSString *)hexFromData : (NSData *) data;
+(NSString *)toHexString:(const void *)data length:(size_t)length space:(bool)space;


@end
