//
//  NSMutableData+NSMutableDataWithConversion.m
//  MyBluetoothTest
//
//  Created by Emmanuel Adeyemi on 1/4/16.
//  Copyright © 2016 Emmanuel Adeyemi. All rights reserved.
//

#import "NSMutableData+NSMutableDataWithConversion.h"

@implementation NSMutableData (NSMutableDataWithConversion)

-(NSString *)hexadecimalString{
    
    const unsigned char *dataBuffer = (const unsigned char *)[self bytes];
    
    if (!dataBuffer)
        return [NSString string];
    
    NSUInteger          dataLength  = [self length];
    NSMutableString     *hexString  = [NSMutableString stringWithCapacity:(dataLength * 2)];
    
    for (int i = 0; i < dataLength; ++i)
        [hexString appendString:[NSString stringWithFormat:@"%02lx", (unsigned long)dataBuffer[i]]];
    
    return [NSString stringWithString:hexString];
}

- (NSString *) hexadecimalCharacters{
    
    NSMutableString *_string = [NSMutableString stringWithString:@""];
    
    NSData *dataAlone = [self subdataWithRange:NSMakeRange(2, [self length] - 2)];
    
    for (int i = 0; i < dataAlone.length; i++) {
        unsigned char _byte;
        [dataAlone getBytes:&_byte range:NSMakeRange(i, 1)];
        if (_byte >= 32 && _byte < 127) {
            [_string appendFormat:@"%c", _byte];
        } else {
            [_string appendFormat:@"[%d]", _byte];
        }
    }
    
    return _string;
}

@end
