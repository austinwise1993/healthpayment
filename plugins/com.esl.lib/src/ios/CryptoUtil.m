//
//  CryptoUtil.m
//  MyBluetoothTest
//
//  Created by Emmanuel Adeyemi on 1/6/16.
//  Copyright © 2016 Emmanuel Adeyemi. All rights reserved.
//

#import "CryptoUtil.h"


@implementation CryptoUtil

-(NSString *) do3DESCBCDecryption : (NSString *) key
                             data : (NSString *) data
                                iv:(NSString *)iv{
    
    NSMutableString *keyString = [[NSMutableString alloc] init];
    
    keyString = [[key substringWithRange:NSMakeRange(0, 32)] mutableCopy];
    
    [keyString appendString:[key substringWithRange:NSMakeRange(0, 16)]];
    
    const void *keyMessage = (const void *)[keyString UTF8String];
    const void *dataMessage = (const void *)[data UTF8String];
    
    void *output;
    
    size_t bufferPtrSize = 0;
    size_t movedBytes = 0;
    
    uint8_t *bufferPtr = NULL;
    
    if(iv == NULL){
        iv = @"00";
    }
    
    //NSData *ivData = [iv dataUsingEncoding:NSUTF8StringEncoding];
    
    uint8_t ivBytes[kCCBlockSize3DES];
    memset((void *) ivBytes, 0x00, (size_t) sizeof(iv));
    
    bufferPtrSize = (data.length + kCCBlockSize3DES) & ~(kCCBlockSize3DES - 1);
    bufferPtr = malloc( bufferPtrSize * sizeof(uint8_t));
    memset((void *)bufferPtr, 0x0, bufferPtrSize);
    
    CCCryptorStatus status = CCCrypt(kCCDecrypt, kCCAlgorithm3DES, kCCOptionECBMode, keyMessage, [key length], ivBytes, dataMessage, [data length], output, bufferPtrSize, &movedBytes);
    
    NSLog(@"Response %d ",status);
    
    NSString *val = *((__unsafe_unretained NSString **)(bufferPtr));
    
    NSLog(@"Decrypted test %@",val);
    
    return val;
}

- (NSString *)do3DESECBDecryption:(NSString *)key data:(NSString *)data{
    
    NSMutableString *keyString = [[NSMutableString alloc] init];
    
    keyString = [[key substringWithRange:NSMakeRange(0, 32)] mutableCopy];
    
    [keyString appendString:[key substringWithRange:NSMakeRange(0, 16)]];
    
    NSData *keyData = [keyString hexStringToData];
    NSData *dataD = [data hexStringToData];
//    
//    const void *keyMessage = (const void *)keyData;
//    const void *dataMessage = (const void *)dataD;
    
//    void *output;
    
    size_t bufferPtrSize = 0;
    size_t movedBytes = 0;
    
    uint8_t *bufferPtr = NULL;
    
    bufferPtrSize = (data.length + kCCBlockSize3DES) & ~(kCCBlockSize3DES - 1);
    bufferPtr = malloc( bufferPtrSize * sizeof(uint8_t));
    memset((void *)bufferPtr, 0x0, bufferPtrSize);
    
    CCCryptorStatus status = CCCrypt(kCCDecrypt, kCCAlgorithm3DES, kCCOptionECBMode, [keyData bytes], [keyData length], NULL, [dataD bytes], [dataD length], bufferPtr, bufferPtrSize, &movedBytes);
    
    
    NSLog(@"size of message moved out %zu",movedBytes);
    NSLog(@"Response %d ",status);
    
    NSData *decryptedData = [[NSData alloc] initWithBytes:bufferPtr length:movedBytes];
    
    NSString *val = [NSString hexFromData:decryptedData];
    
    NSLog(@"Decrypted test %@",val);
    
    return val;
}

-(NSString *)do3DESECBEncryption:(NSString *)key
                            data:(NSString *)data {
    
    NSMutableString *keyString = [[NSMutableString alloc] init];
    
    keyString = [[key substringWithRange:NSMakeRange(0, 32)] mutableCopy];
    
    [keyString appendString:[key substringWithRange:NSMakeRange(0, 16)]];
    
    
    const void *keyMessage = (const void *)[keyString UTF8String];
    const void *dataMessage = (const void *)[data UTF8String];
    
    void *output;
    
    size_t bufferPtrSize = 0;
    size_t movedBytes = 0;
    
    uint8_t *bufferPtr = NULL;
    
    bufferPtrSize = (data.length + kCCBlockSize3DES) & ~(kCCBlockSize3DES - 1);
    bufferPtr = malloc( bufferPtrSize * sizeof(uint8_t));
    memset((void *)bufferPtr, 0x0, bufferPtrSize);
    
    CCCryptorStatus status = CCCrypt(kCCEncrypt, kCCAlgorithm3DES, kCCOptionECBMode, keyMessage, [key length], NULL, dataMessage, [data length], output, bufferPtrSize, &movedBytes);
    
//    if(status ==)
    
    NSLog(@"Response %d ",status);
    
    NSString *val = *((__unsafe_unretained NSString **)(bufferPtr));
    
    NSLog(@"Decrypted test %@",val);
    
    return val;
}

@end
