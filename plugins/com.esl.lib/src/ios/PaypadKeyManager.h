//
//  PaypadKeyManager.h
//  MyBluetoothTest
//
//  Created by Emmanuel Adeyemi on 1/13/16.
//  Copyright © 2016 Emmanuel Adeyemi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DTDevices.h"
#import "NSString+HexUtil.h"
#import "Global.h"

extern NSString *const DUKPT_TR31_LOCATION;
extern NSString *const DATA_TR31_LOCATION;
extern NSString *const TMK_TR31_LOCATION;
extern NSString *const BDK_LOCATION;
extern NSString *const TSK_LOCATION;
extern NSString *const TPK_LOCATION;
extern NSString *const DATA_KEY_LOCATION;
extern NSString *const ACCEPTOR_ID;
extern NSString *const MERCHANT_TYPE;
extern NSString *const ACCEPTOR_NAME;
// This section is for VAS
extern NSString *const ACCEPTOR_ID_VAS;
extern NSString *const MERCHANT_TYPE_VAS;
extern NSString *const ACCEPTOR_NAME_VAS;
extern NSString *const TSK_VAS_LOCATION;
extern NSString *const TPK_VAS_LOCATION;
extern NSString *const TMK_TR31_VAS_LOCATION;


extern int KEY_IPEK_INDEX;
extern int KEY_DATA_INDEX;
extern int KEY_PIN_INDEX;
extern int KEY_PIN_VAS_INDEX;

@interface PaypadKeyManager : NSObject{
    DTDevices *dtdev;
}

-(id) initWithDTDevices : (DTDevices*) dtdevice;
-(void) loadCAKeys ;
-(void) loadDownloadedKeys;
-(void) loadKeys;

@end
