import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LoginPage } from '../login/login';


@IonicPage()
@Component({
  selector: 'page-activation',
  templateUrl: 'activation.html',
})
export class ActivationPage {

  posts: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  loginPg() {

    this.navCtrl.push(LoginPage)
    // alert('it works');
  }
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad ActivationPage');
  }

}
