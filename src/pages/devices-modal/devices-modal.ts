import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Navbar,ViewController, LoadingController, AlertController, ModalController } from 'ionic-angular';
import { PaymentPage } from '../payment/payment';
import { InsertcardPage } from '../insertcard/insertcard';
import { EnterPinPage } from '../enter-pin/enter-pin';
import { ResultsPage } from '../results/results';

declare var PaypadFacade: any;

@IonicPage()
@Component({
  selector: 'page-devices-modal',
  templateUrl: 'devices-modal.html',
})
export class DevicesModalPage {

  @ViewChild('navbar') navBar: Navbar;

  back() {
    this.navCtrl.push(PaymentPage);
  }

  dataDevices: Array<any> = [];
  deviceList: Array<any>;

  constructor(public modalCtrl: ModalController, public navCtrl: NavController, private alertCtrl: AlertController, 
    public loading: LoadingController, public navParams: NavParams, public viewCtrl: ViewController) {
  }

  ionViewWillEnter() {
    this.viewCtrl.showBackButton(true);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DevicesModalPage');
    // console.log(JSON.stringify(this.navParams.get('obj')));
    this.deviceList = this.navParams.get('obj');
  }

  // ionViewDidEnter() {
  //   this.navBar.backButtonClick = () => {
  //     this.navCtrl.push(PaymentPage);
  //   };
  // }

  bluetoothfunc(name, address) {

    console.log('bluetooth address', address);
    console.log("Got here")
    var loader = this.loading.create({
      content: '<ion-spinner name="dots"></ion-spinner> <br />',
    });

    PaypadFacade.connection(address, (response) => {

      console.log('status ', response.status, 'address', address);

      var status = response.status;

      if (status) {

        if (status === "enabling_bluetooth") {
          loader = this.loading.create({
            content: '<ion-spinner name="dots"></ion-spinner> <br />Please wait enabling bluetooth...',
          });
          loader.present()
        }
        else if (status === "connecting") {
          loader.dismiss()
          console.log('connecting ', status);
          loader = this.loading.create({
            content: '<ion-spinner name="dots"></ion-spinner> <br /> Connecting to pinpad...',
          });

          loader.present()
        } else if (status === "connected") {

          loader.dismiss()

          console.log("connected from devices page")

          localStorage.setItem("agree", "true");


          // var x = "2030ES03";
          var x = "2063073T";

          // alert('the terminal Id is ' + x);
          var selectedbank = "Esl";
          PaypadFacade.initialization(x, true, selectedbank, (response) => {
            console.log(response)
            var status = response.status;
            // alert('Initialization status '+ status);
            if (status) {
              if (status === "downloading_keys") {
                loader = this.loading.create({
                  content: '<ion-spinner name="dots"></ion-spinner> <br />Please wait, downloading keys...',
                });
                loader.present()

              } else if (status === "loading_keys_to_pinpad") {

                loader.dismiss();

                let alert = this.alertCtrl.create({
                  title: 'Status',
                  subTitle: 'KEY DOWNLOAD SUCCESSFUL',
                  buttons: ['Dismiss']
                });
                alert.present();

                localStorage.setItem("initialization", "true");

                console.log('called payment from devices page')

                this.payment();

              } else if (status == "Already_initialised") {
                
                alert("already initialized");
                         
              }
            }

          }, (err) => {
            console.log(err)
            loader.dismiss();

            let alert = this.alertCtrl.create({
              title: 'Status',
              subTitle: 'Unable to Download Keys',
              buttons: ['Dismiss']
            });

            alert.present();

            localStorage.setItem("initialization", "false");

          });
        } else {
          loader.dismiss();
        }
      }

    }, (response) => {
      // alert("Error "+JSON.stringify(response));
      loader.dismiss();

      localStorage.setItem("initialization", "false");
      
      let alert = this.alertCtrl.create({
        title: 'Status',
        subTitle: 'Unable to Connect to Pinpad',
        buttons: ['Dismiss']
      });

      alert.present();

    });
  };

  payment() {

    // console.log("Got here")

    this.navCtrl.push(InsertcardPage)

    var loader = this.loading.create({
      content: '<ion-spinner name="dots"></ion-spinner> <br />',
    });
      
    PaypadFacade.payment({ "amount": localStorage.getItem('Amount'), "account": "savings" }, (data) => {

      // console.log("data from payment")
      // console.log(data);

      if (data.operation === "payment") {

        if (data.status === "processing") {

          loader = this.loading.create({
            content: '<ion-spinner name="dots"></ion-spinner> <br />Processing...',
          });
          loader.present();

        } else if (data.status === "enterPin") {

          this.navCtrl.push(EnterPinPage);

          loader.dismiss();

        } else {

          this.navCtrl.push(ResultsPage, { "obj": data })

          loader.dismiss();
        }
      }

    }, (error) => {
      loader.dismiss();
      console.log(error);
    });
  };

  ionViewDidEnter() {
    this.navBar.backButtonClick = () => {
    };
  }

  doRefresh(refresher: { complete: () => void; }) {
    console.log('Started', refresher);
    setTimeout(() => {
        console.log('Async operation has ended');
        refresher.complete();
    }, 4000);
  }

}
