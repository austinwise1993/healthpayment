import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Navbar } from 'ionic-angular';
// import { PaymentPage } from '../payment/payment';



@IonicPage()
@Component({
  selector: 'page-insertcard',
  templateUrl: 'insertcard.html',
})
export class InsertcardPage {

  @ViewChild('navbar') navBar: Navbar;

  // back() {
  //   this.navCtrl.push(PaymentPage);
  // }

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  // ionViewDidEnter() {
  //   this.navBar.backButtonClick = () => {
  //     here you can do wathever you want to replace the backbutton event
  //     this.navCtrl.push(PaymentPage);
  //   };
  // }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InsertcardPage');
  }

}
