import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InsertcardPage } from './insertcard';

@NgModule({
  declarations: [
    InsertcardPage,
  ],
  imports: [
    IonicPageModule.forChild(InsertcardPage),
  ],
})
export class InsertcardPageModule {}
