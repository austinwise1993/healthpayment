import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, AlertController, LoadingController  } from 'ionic-angular';
import { InsertcardPage } from '../insertcard/insertcard';
import { EnterPinPage } from '../enter-pin/enter-pin';
import { DevicesModalPage } from '../devices-modal/devices-modal';
import { ResultsPage } from '../results/results';

// Declare an interface and pass the object in an array 
declare var PaypadFacade: any;
interface AddInfo {
  Value: string;
  Name: string;
}

interface Invoice {
  id: string;
  invoiceId: string;
  itemId: string;
  itemDescription: string;
  entryTime: string;
  amount: string;
}
// Declarng interface end

@IonicPage()
@Component({
selector: 'page-payment',
templateUrl: 'payment.html',
})
export class PaymentPage {

InvoiceId: string;
InvoiceNumber: string;
InvoiceStatus: string;
Amount: string;
OutstandingAmount: string;
PaidAmount: string;
CurrencyCode: string;
RevenueCode: string;
RevenueName: string;
CorporateCode: string;
CorporateName: string;
InvoiceReference: string;

//declaring a variable of type array
AdditionalInformation: Array<AddInfo>;
InvoiceDetails: Array<Invoice>;
//declaring a variable of type array

constructor(public modalCtrl: ModalController, private alertCtrl: AlertController, public navCtrl: NavController, 
public navParams: NavParams, public loading: LoadingController) {

}

insertCd() {
  this.navCtrl.push(InsertcardPage)
// alert('it works');
}

ionViewDidLoad() {
this.InvoiceId = localStorage.getItem('InvoiceId');
this.InvoiceNumber = localStorage.getItem('InvoiceNumber');
this.InvoiceStatus = localStorage.getItem('InvoiceStatus');
this.Amount = localStorage.getItem('Amount');
this.OutstandingAmount = localStorage.getItem('OutstandingAmount');
this.PaidAmount = localStorage.getItem('PaidAmount');
this.CurrencyCode = localStorage.getItem('CurrencyCode');
this.RevenueCode = localStorage.getItem('RevenueCode');
this.RevenueName = localStorage.getItem('RevenueName');
this.CorporateCode = localStorage.getItem('CorporateCode');
this.CorporateName = localStorage.getItem('CorporateName');
this.InvoiceReference = localStorage.getItem('InvoiceReference');

// use a jsonpCallbackContext.parse() to parse your array and getItem() to get items,and also 
// store in a localStorage for later use
// Remember to setItem to where API is being called, i.e invoice.ts in our own case
//Finally use *ngFor to bind and call the objetcs in this array, withing payment.html
this.AdditionalInformation = JSON.parse(localStorage.getItem('AdditionalInformation'));
this.InvoiceDetails = JSON.parse(localStorage.getItem('InvoiceDetails'));
}
openModal(data) {
  let myModal = this.modalCtrl.create(DevicesModalPage, { 'obj': data });
  myModal.present();
}

connectToPinpadAndDownloadKeys() {

  var success = (message) => {

    if (message) {

      var myPopup = null;
      var deviceList = message.devices;
      this.openModal(deviceList);

    } else {

      let alert = this.alertCtrl.create({
        title: 'Important',
        subTitle: 'Unable to complete operation, please try again later',
        buttons: ['Dismiss']
      });

      alert.present();
    }
  }

  var failure = (message) => {

    let alert = this.alertCtrl.create({
      title: 'Important',
      subTitle: 'Unable to complete operation, please try again later',
      buttons: ['Dismiss']
    });

    alert.present();
  };

  var loader = this.loading.create({
    content: '<ion-spinner name="dots"></ion-spinner><br />',
  });

  PaypadFacade.connection("", (response) => {

    console.log(response)

    var status = response.status;

    if (status) {

      if (status === "enabling_bluetooth") {
        loader = this.loading.create({
          content: '<ion-spinner name="dots"></ion-spinner> <br />Please wait enabling bluetooth...',
        });
        loader.present()
        
      }
      else if (status == "connecting") {
        loader.dismiss();
        loader = this.loading.create({
          content: '<ion-spinner name="dots"></ion-spinner> <br /> Connecting to pinpad...',
        });

        loader.present()
        
      } else if (status === "connected" || status === "already_connected") {

        var x = "2030ES03";

        //console.log("i am connected from payment page")

        loader.dismiss();

        var selectedbank = "Esl";
        var initState = localStorage.getItem("initialization");

        console.log(initState);

        if(initState === "false"){

        PaypadFacade.reinitialization(x, true, selectedbank, (result) => {

          console.log(result);

          var status = result.status;

          if (status) {

            if (status === "downloading_keys") {
              loader = this.loading.create({
                content: '<ion-spinner name="dots"></ion-spinner> <br />Please wait downloading keys...',
              });
              loader.present()

            } else if (status === "loading_keys_to_pinpad") {

              loader.dismiss();

              console.log('called payment from payment page')

              // alert("KEY DOWNLOAD SUCCESSFUL")

              let alert = this.alertCtrl.create({
                title: 'Status',
                subTitle: 'KEY DOWNLOAD SUCCESSFUL',
                buttons: ['Dismiss']
              });

              alert.present();

              localStorage.setItem("initialization", "true");

              this.payment();
              
            }
            else if (status == "Already_initialised") {

              alert("already initialized");

            }
          }

        },(error) => {
          console.log('Unable to download keys from connectToPinpad')
          console.log(error)
          loader.dismiss();

          let alert = this.alertCtrl.create({
            title: 'Status',
            subTitle: 'Unable to Download Keys',
            buttons: ['Dismiss']
          });

          alert.present();

         });      
         
         localStorage.setItem("initialization", "false");

        } else{
            this.payment();
        }
      } else {

        loader.dismiss();
        // alert("This is else "+ status);
        console.log("listing")
        PaypadFacade.list(success, failure);
      }
    } else {
      // PaypadFacade.list(success,failure);//wise 
      this.payment();
      console.log("not status");
      console.log(status)
      //reject("no response status from connection")
    }

  }, (response) => {

    loader.dismiss();

    PaypadFacade.list(success, failure);

  });

}

payment() {

  this.navCtrl.push(InsertcardPage);

  var loader = this.loading.create({
    content: '<ion-spinner name="dots"></ion-spinner> <br />',
  });

  PaypadFacade.payment(
    {
     "amount": localStorage.getItem('Amount'),
    //  "account": "savings"
    }, (data) => {

    console.log(data);

    if (data.operation === "payment") {

      if (data.status === "processing") {

        loader = this.loading.create({
          content: '<ion-spinner name="dots"></ion-spinner> <br />Processing...',
        });

        loader.present();

      } else if (data.status === "enterPin") {
        loader.dismiss();

        this.navCtrl.push(EnterPinPage)

        loader.dismiss();

      } else {

        this.navCtrl.push(ResultsPage, { "obj": data })

        loader.dismiss();
      }
    }

  }, (error) => {
    loader.dismiss();
    // this.navCtrl.push(InvoicePage)
    console.log(error);
  })
  
}

doRefresh(refresher: { complete: () => void; }) {
  console.log('Started', refresher);
  setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
  }, 4000);
}

}
