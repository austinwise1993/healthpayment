import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Navbar } from 'ionic-angular';
// import { PaymentPage } from '../payment/payment';

@IonicPage()
@Component({
  selector: 'page-enter-pin',
  templateUrl: 'enter-pin.html',
})
export class EnterPinPage {

  @ViewChild('navbar') navBar: Navbar;

  // back() {
  //   this.navCtrl.push(PaymentPage);
  // }

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EnterPinPage');
  }

}
