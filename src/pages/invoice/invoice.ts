import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Navbar, Header, ToastController } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core'
import { ConfirmPage } from '../confirm/confirm';


@IonicPage()
@Component({
  selector: 'page-invoice',
  templateUrl: 'invoice.html',
})
@Injectable()
export class InvoicePage {

  @ViewChild('navbar') navBar: Navbar;

  posts: string;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public loadingCtrl: LoadingController, private http: HttpClient, public toastCtrl: ToastController) {

  }

  confirmPg() {

    if (this.posts == undefined) {

      let toast = this.toastCtrl.create({
        message: 'Please enter your invoice number',
        duration: 3000,
        position: 'top',
        cssClass: 'normalToast'

      });

      toast.present();
      return;
    }
    const loader = this.loadingCtrl.create({
      content: '<ion-spinner name="dots"></ion-spinner> Please wait...',

    });
    loader.present();


    //Http Request
    this.http.get('http://154.113.17.180:9011/api/v1/pos/invoicing/validateinvoicenumber?invoiceNumber='
      + this.posts).subscribe((res: any) => {
        console.log('got here')
        console.log(res);

        localStorage.setItem('InvoiceId', res.InvoiceId);
        localStorage.setItem('InvoiceNumber', res.InvoiceNumber);
        localStorage.setItem('InvoiceStatus', res.InvoiceStatus);
        localStorage.setItem('Amount', res.Amount);
        localStorage.setItem('OutstandingAmount', res.OutstandingAmount);
        localStorage.setItem('PaidAmount', res.PaidAmount);
        localStorage.setItem('CurrencyCode', res.CurrencyCode ? res.CurrencyCode : '');
        localStorage.setItem('RevenueCode', res.RevenueCode ? res.RevenueCode : '');
        localStorage.setItem('CorporateCode', res.CorporateCode);
        localStorage.setItem('RevenueName', res.RevenueName);
        localStorage.setItem('InvoiceReference', res.InvoiceReference);
        localStorage.setItem('CorporateName', res.CorporateName);

        //Storing and setting the variable 
        localStorage.setItem('AdditionalInformation', JSON.stringify(res.AdditionalInformation));
        localStorage.setItem('InvoiceDetails', JSON.stringify(res.InvoiceDetails));
        this.navCtrl.setRoot(ConfirmPage);
        loader.dismiss();

      }, (data: any) => {
        loader.dismiss();
        console.log(data)
        let toast = this.toastCtrl.create({
          message: data.error.Message,
          duration: 3000,
          position: 'top',
          cssClass: 'normalToast'

        });

        toast.present();
        console.log('hello')
      })


  }
  //Http Request end

  ionViewDidEnter() {
    this.navBar.backButtonClick = () => {
    };
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad InvoicePage');
  }

}
