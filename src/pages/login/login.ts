import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { InvoicePage } from '../invoice/invoice';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  passwordType: string = 'password';
  passwordShown: boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  public togglePasword(){
    if(this.passwordShown){
      this.passwordShown = false;
      this.passwordType = 'password';
    } else {
      this.passwordShown = true;
      this.passwordType = 'text';
    }
  }

  invoicePg() {
    this.navCtrl.push(InvoicePage)
    // alert('it works')
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

}
