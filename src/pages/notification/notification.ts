import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, } from 'ionic-angular';
// import { PhonegapLocalNotification } from '@ionic-native/phonegap-local-notification';

@IonicPage()
@Component({
  selector: 'page-notification',
  templateUrl: 'notification.html',
})
export class NotificationPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NotificationPage');
  }

}
