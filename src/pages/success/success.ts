import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { InvoicePage } from '../invoice/invoice';

@IonicPage()
@Component({
  selector: 'page-success',
  templateUrl: 'success.html',
})
export class SuccessPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  returnHome() {
    this.navCtrl.push(InvoicePage)
    // alert('it works');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SuccessPage');
  }

}
