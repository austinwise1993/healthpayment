import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { PaymentPage } from '../payment/payment';
import { HTTP } from '@ionic-native/http';
import * as moment from 'moment';

@IonicPage()
@Component({
  selector: 'page-results',
  templateUrl: 'results.html',
})
export class ResultsPage {
    
  InvoiceId: string;
  InvoiceNumber: string;
  InvoiceStatus: string;
  Amount: string;
  OutstandingAmount: string;
  PaidAmount: string;
  CurrencyCode: string;
  RevenueCode: string;
  RevenueName: string;
  CorporateCode: string;
  CorporateName: string;

  resultImage: string = "";
  pan: string; 
  cardHolder: string; 
  stan: string; 
  rrn: string;
  expiry: string; 
  responseMessage: string; 
  responseCode: string;
  tenderType: string; 
  accountType: string;

  date: string;

  constructor(public navCtrl: NavController,private http: HttpClient,public nativeHttp:HTTP, public navParams: NavParams, public viewCtrl: ViewController) {
  }

  ionViewWillEnter() {
    this.viewCtrl.showBackButton(false);
  }

  ionViewDidLoad() {
    
    console.log('ionViewDidLoad ResultsPage');
    const result = this.navParams.get('obj');

    const d = new Date();
    const momentDateTime = moment(d).format("YYYY-MM-DD HH:mm:ss");
    document.getElementById("date").innerHTML = momentDateTime;

    // this.date = new Date().toISOString();

    if (result.responsecode == "00") {
      this.resultImage = "assets/imgs/success.svg";
    } else {
      this.resultImage = "assets/imgs/failed.svg";
    }
    this.pan = result.pan;
    this.cardHolder = result.cardholder;
    this.stan = result.stan;
    this.rrn = result.rrn;
    this.responseCode = result.responsecode;
    this.responseMessage = result.responsemessage;
    // this.accountType = result.accounttype;
    this.tenderType = result.tendertype;

    const expiry = result.expiry;
    expiry.split("");
    const formattedDate = expiry[2] + "" + expiry[3] + "/" + expiry[0] + "" + expiry[1];
    this.expiry = formattedDate;
    
    let data = {           
      Amount: localStorage.getItem('Amount'),
      CardScheme: result.tendertype,
      CurrencyCode:localStorage.getItem('CurrencyCode'),
      CustomerName: result.cardholder,
      MaskedPAN: result.pan,
      PaymentDate: momentDateTime,
      Reference: localStorage.getItem('InvoiceNumber'),
      RetrievalReferenceNumber: result.rrn,
      StatusCode: result.responsecode,
      StatusDescription: result.responsemessage ,
      TransactionReference: localStorage.getItem('InvoiceReference'),
      type: result.tendertype ,
    }
    
    console.log(data);
    this.InvoiceNumber = localStorage.getItem('InvoiceNumber');
    console.log('Before request: ');

    try {
      this.nativeHttp.setDataSerializer('json');
      this.nativeHttp.post('http://154.113.17.180:9011/api/v1/pos/invoicing/invoicepayment?invoiceNumber=' + this.InvoiceNumber, data,{ 
      'TerminalID': '2063073T',  
      'ProviderID': '02',
      'Content-Type': 'application/json'
      }).then(res => {
        console.log('request success: ');
        console.log(res);
        alert(res)
      }).catch(e => {
        console.log('request fail: ');
        console.log(e);
      });
    }catch(e) {
      console.log(e.message);             
      console.log(e.name);                 
      console.log(e.fileName);             
      console.log(e.lineNumber);          
      console.log(e.columnNumber);        
      console.log(e.stack);
      console.error('Request error: ', e);
    }
    console.log('After request: ');
  }

  newTransaction() {
    this.navCtrl.setRoot(PaymentPage) 
  }

}
