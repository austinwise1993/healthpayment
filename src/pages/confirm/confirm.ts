import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { InvoicePage } from '../invoice/invoice';
import { PaymentPage } from '../payment/payment';

interface Xyz {
  Value: string;
  Name: string;
}

@IonicPage()
@Component({
  selector: 'page-confirm',
  templateUrl: 'confirm.html',
})
export class ConfirmPage {


  InvoiceId: string;
  InvoiceNumber: string;
  InvoiceStatus: string;
  Amount: string;
  OutstandingAmount: string;
  PaidAmount: string;
  CurrencyCode: string;
  RevenueCode: string;
  RevenueName: string;
  CorporateCode: string;
  CorporateName: string;
  InvoiceReference: string;
  AdditionalInformation: Array<Xyz>;
  
  
  
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  pay() {
    this.navCtrl.push(PaymentPage)
    // alert('it works')
    // console.log(this.AdditionalInformation[0]['Value'], this.AdditionalInformation[0]['Name'])
  }

  invoicePg() {
    this.navCtrl.push(InvoicePage)
    // alert('it works')
  }

  ionViewDidLoad() {
    this.InvoiceId = localStorage.getItem('InvoiceId');
    this.InvoiceNumber = localStorage.getItem('InvoiceNumber');
    this.InvoiceStatus = localStorage.getItem('InvoiceStatus');
    this.Amount = localStorage.getItem('Amount');
    this.OutstandingAmount = localStorage.getItem('OutstandingAmount');
    this.PaidAmount = localStorage.getItem('PaidAmount');
    this.CurrencyCode = localStorage.getItem('CurrencyCode');
    this.RevenueCode = localStorage.getItem('RevenueCode');
    this.RevenueName = localStorage.getItem('RevenueName');
    this.CorporateCode = localStorage.getItem('CorporateCode');
    this.CorporateName = localStorage.getItem('CorporateName');
  }

}
