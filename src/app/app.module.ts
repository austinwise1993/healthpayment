import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { InsertcardPage } from '../pages/insertcard/insertcard';
import { SuccessPage } from '../pages/success/success';
import { ActivationPage } from '../pages/activation/activation';
import { ConfirmPage } from '../pages/confirm/confirm';
import { InvoicePage } from '../pages/invoice/invoice';
import { PaymentPage } from '../pages/payment/payment';
import { LoginPage } from '../pages/login/login';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HttpClientModule } from '@angular/common/http';
import { DevicesModalPage } from '../pages/devices-modal/devices-modal';
import { ResultsPage } from '../pages/results/results';
import { EnterPinPage } from '../pages/enter-pin/enter-pin';
// import { SelectAccountTypePage } from '../pages/select-account-type/select-account-type';
import { HTTP } from '@ionic-native/http';

@NgModule({
  declarations: [
    MyApp,
    InsertcardPage,
    EnterPinPage,
    SuccessPage,
    ActivationPage,
    ConfirmPage, 
    InvoicePage,
    DevicesModalPage,
    PaymentPage,
    LoginPage,
    ResultsPage,
    // SelectAccountTypePage
     
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    InsertcardPage,
    EnterPinPage,
    SuccessPage,
    ActivationPage,
    ConfirmPage, 
    InvoicePage,
    DevicesModalPage,
    PaymentPage,
    LoginPage,
    ResultsPage,
    // SelectAccountTypePage
    
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    HttpClientModule,
    HTTP,
  ]
})
export class AppModule {}
