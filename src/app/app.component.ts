import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, App, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { timer } from 'rxjs/observable/timer';

import { InvoicePage } from '../pages/invoice/invoice';
import { PaymentPage } from '../pages/payment/payment';
import { ConfirmPage } from '../pages/confirm/confirm';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = InvoicePage;

  //...omitted for spinner
  showSplash = true; // show animation
  //spinner end


  pages: Array<{title: string, component: any}>;
  // data: { title: string; description: string; date: string; time: string; };

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, 
    public app: App, public alertCtrl: AlertController) {

    platform.ready().then(() => {

      platform.registerBackButtonAction(() => {

        let nav = this.app.getActiveNavs()[0];
        let activeView = nav.getActive();

        // Checks if can go back before show up the alert
        if (activeView.name === 'ConfirmPage') {

          this.nav.setRoot(InvoicePage);

        } 
        if (activeView.name === 'PaymentPage') {

          this.nav.setRoot(ConfirmPage);

        }
         if (activeView.name === 'InsertcardPage' || activeView.name === 'EnterPinPage') {
          const confirm = this.alertCtrl.create({
            title: 'Exit',
            message: 'Do you want to Exit from the app?',
            buttons: [
              {
                text: 'Cancel',
                handler: () => {
                  return;
                }
              },
              {
                text: 'Confirm',
                handler: () => {
                  platform.exitApp();
                }
              }
            ]
          });

          confirm.present();

        } 
        if (activeView.name === 'DevicesModalPage') {

          const confirm = this.alertCtrl.create({
            title: 'Exit',
            message: 'Do you want to Exit from the app?',
            buttons: [
              {
                text: 'Cancel',
                handler: () => {
                  return;
                }
              },
              {
                text: 'Confirm',
                handler: () => {
                  platform.exitApp();
                }
              }
            ]
          });
          confirm.present();
        }
        if (activeView.name === 'ResultsPage') {

          const confirm = this.alertCtrl.create({
            title: 'Exit',
            message: 'Are you sure you want to exit the app?',
            buttons: [
              {
                text: 'Cancel',
                handler: () => {
                  
                  return;
                }
              },
              {
                text: 'Confirm',
                handler: () => {
                  platform.exitApp();
                }
              }
            ]
          });

          confirm.present();

        }
        else if (activeView.name === 'InvoicePage') {

          const confirm = this.alertCtrl.create({
            title: 'Exit',
            message: 'Are you sure you want to exit the app?',
            buttons: [
              {
                text: 'Cancel',
                handler: () => {
                  
                  return;
                }
              },
              {
                text: 'Confirm',
                handler: () => {
                  platform.exitApp();
                }
              }
            ]
          });

          confirm.present();

        }

      });

      statusBar.styleDefault();
      splashScreen.hide();

    });

    this.initializeApp();

    // this.data = { title:'', description:'', date:'', time:'' };

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Payment', component: PaymentPage },
      { title: 'Invoice', component: InvoicePage },
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      timer(4000).subscribe(() => this.showSplash = false) // hide animation after 4s
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  
}
